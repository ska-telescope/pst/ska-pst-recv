/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <string>
#include <sstream>
#include <thread>
#include <spdlog/spdlog.h>

#include "ska/pst/common/lmc/LmcServiceException.h"
#include "ska/pst/recv/lmc/RecvLmcServiceHandler.h"

auto beam_configuration_as_ascii_header(
    const ska::pst::lmc::BeamConfiguration &configuration
) -> ska::pst::common::AsciiHeader
{
    SPDLOG_TRACE("ska::pst::recv::beam_configuration_as_ascii_header()");
    // assert we only have an recv configure beam request
    if (!configuration.has_receive()) {
        SPDLOG_WARN("BeamConfiguration protobuf message has no RECV details provided.");
        throw ska::pst::common::LmcServiceException(
            "Expected a RECV beam configuration object, but none were provided.",
            ska::pst::lmc::ErrorCode::INVALID_REQUEST,
            grpc::StatusCode::INVALID_ARGUMENT
        );
    }

    const auto &recv_resources = configuration.receive();
    const auto &subband_resources = recv_resources.subband_resources();

    ska::pst::common::AsciiHeader config;
    int timeout = 0;

    config.set("UDP_NSAMP", recv_resources.udp_nsamp());
    config.set("WT_NSAMP", recv_resources.wt_nsamp());
    config.set("UDP_NCHAN", recv_resources.udp_nchan());
    config.set("FRONTEND", recv_resources.frontend());
    config.set("FD_POLN", recv_resources.fd_poln());
    config.set("FD_HAND", recv_resources.fd_hand());
    config.set("FD_SANG", recv_resources.fd_sang());
    config.set("FD_MODE", recv_resources.fd_mode());
    config.set("FA_REQ", recv_resources.fa_req());
    config.set("NANT", recv_resources.nant());
    config.set("ANTENNAE", recv_resources.antennas());
    config.set("ANT_WEIGHTS", recv_resources.ant_weights());
    config.set("NPOL", recv_resources.npol());
    config.set("NBIT", recv_resources.nbits());
    config.set("NDIM", recv_resources.ndim());
    config.set("TSAMP", recv_resources.tsamp());
    config.set("OS_FACTOR", recv_resources.ovrsamp());
    config.set("NSUBBAND", recv_resources.nsubband());
    config.set("UDP_FORMAT", recv_resources.udp_format());
    config.set("BYTES_PER_SECOND", recv_resources.bytes_per_second());
    config.set("BEAM_ID", recv_resources.beam_id());

    config.set("DATA_KEY", subband_resources.data_key());
    config.set("WEIGHTS_KEY", subband_resources.weights_key());
    config.set("BW", subband_resources.bandwidth());
    config.set("NCHAN", subband_resources.nchan());
    config.set("START_CHANNEL", subband_resources.start_channel());
    config.set("END_CHANNEL", subband_resources.end_channel());
    config.set("FREQ", subband_resources.frequency());
    config.set("START_CHANNEL_OUT", subband_resources.start_channel_out());
    config.set("END_CHANNEL_OUT", subband_resources.end_channel_out());
    config.set("NCHAN_OUT", subband_resources.nchan_out());
    config.set("BW_OUT", subband_resources.bandwidth_out());
    config.set("FREQ_OUT", subband_resources.frequency_out());
    config.set("DATA_HOST", subband_resources.data_host());
    config.set("DATA_PORT", subband_resources.data_port());

    return config;
}

auto scan_configuration_as_ascii_header(
    const ska::pst::lmc::ScanConfiguration& scan_configuration
) -> ska::pst::common::AsciiHeader
{
    SPDLOG_TRACE("ska::pst::recv::scan_configuration_as_ascii_header()");
    // ensure that this request was for RECV, even if it was an empty no-op
    if (!scan_configuration.has_receive()) {
        SPDLOG_WARN("ScanConfiguration protobuf message has no RECV details provided.");

        throw ska::pst::common::LmcServiceException(
            "Expected a RECV scan configuration object, but none were provided.",
            ska::pst::lmc::ErrorCode::INVALID_REQUEST,
            grpc::StatusCode::INVALID_ARGUMENT
        );
    }

    ska::pst::common::AsciiHeader header;
    const auto &configuration = scan_configuration.receive();
    header.set("ACTIVATION_TIME", configuration.activation_time());
    header.set("OBSERVER", configuration.observer());
    header.set("PROJID", configuration.projid());
    header.set("PNT_ID", configuration.pnt_id());
    header.set("SUBARRAY_ID", configuration.subarray_id());
    header.set("SOURCE", configuration.source());
    header.set("ITRF", configuration.itrf());
    // protobuf's default value for numbers is 0.
    // if both are zero then assume they were not set.
    if (configuration.bmaj() != 0.0 and configuration.bmin() != 0.0)
    {
        header.set("BMAJ", configuration.bmaj());
        header.set("BMIN", configuration.bmin());
    }
    header.set("COORD_MD", configuration.coord_md());
    header.set("EQUINOX", configuration.equinox());
    header.set("STT_CRD1", configuration.stt_crd1());
    header.set("STT_CRD2", configuration.stt_crd2());
    header.set("TRK_MODE", configuration.trk_mode());
    header.set("SCANLEN_MAX", configuration.scanlen_max());
    // set default OBS_OFFSET
    header.set("OBS_OFFSET", 0);
    if (configuration.test_vector().length() > 0)
    {
        header.set("TEST_VECTOR", configuration.test_vector());
    }
    header.set("EB_ID", configuration.execution_block_id());

    return header;
}

void ska::pst::recv::RecvLmcServiceHandler::validate_beam_configuration(
    const ska::pst::lmc::BeamConfiguration& configuration
)
{
    auto config = beam_configuration_as_ascii_header(configuration);
    ska::pst::common::ValidationContext context;
    recv->validate_configure_beam(config, &context);
    context.throw_error_if_not_empty();
}

void ska::pst::recv::RecvLmcServiceHandler::configure_beam(
    const ska::pst::lmc::BeamConfiguration& configuration
)
{
    SPDLOG_TRACE("ska::pst::recv::RecvLmcServiceHandler::configure_beam()");

    // check if data manager has already have had beam configured
    if (recv->is_beam_configured()) {
        SPDLOG_WARN("Received configure beam request but beam configured already.");
        throw ska::pst::common::LmcServiceException(
            "Resources already assigned for RECV.",
            ska::pst::lmc::ErrorCode::CONFIGURED_FOR_BEAM_ALREADY,
            grpc::StatusCode::FAILED_PRECONDITION
        );
    }

    auto config = beam_configuration_as_ascii_header(configuration);
    // validation will happens on the state model as first part of configure_beam
    // so no need to do validation here.
    recv->configure_beam(config);

    SPDLOG_TRACE("Finish configuring beam");
}

void ska::pst::recv::RecvLmcServiceHandler::deconfigure_beam()
{
    SPDLOG_TRACE("ska::pst::recv::RecvLmcServiceHandler::deconfigure_beam()");

    // check if receiver has already have had beam configured
    if (!recv->is_beam_configured()) {
        SPDLOG_WARN("Received request to deconfigure beam when beam not configured.");
        throw ska::pst::common::LmcServiceException(
            "RECV not configured for beam.",
            ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_BEAM,
            grpc::StatusCode::FAILED_PRECONDITION
        );
    }

    recv->deconfigure_beam();
}

void ska::pst::recv::RecvLmcServiceHandler::get_beam_configuration(
    ska::pst::lmc::BeamConfiguration* response
)
{
    SPDLOG_TRACE("ska::pst::recv::RecvLmcServiceHandler::get_beam_configuration()");
    if (!recv->is_beam_configured())
    {
        SPDLOG_WARN("Received request to get beam configuration when beam not configured.");
        throw ska::pst::common::LmcServiceException(
            "RECV not configured for beam.",
            ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_BEAM,
            grpc::StatusCode::FAILED_PRECONDITION
        );
    }

    const auto& config = recv->get_beam_configuration();

    ska::pst::lmc::ReceiveBeamConfiguration *recv_resources = response->mutable_receive();
    ska::pst::lmc::ReceiveSubbandResources *subband_resources =
        recv_resources->mutable_subband_resources();

    recv_resources->set_udp_nsamp(config.get_uint32("UDP_NSAMP"));
    recv_resources->set_wt_nsamp(config.get_uint32("WT_NSAMP"));
    recv_resources->set_udp_nchan(config.get_uint32("UDP_NCHAN"));
    recv_resources->set_frontend(config.get_val("FRONTEND"));
    recv_resources->set_fd_poln(config.get_val("FD_POLN"));
    recv_resources->set_fd_hand(config.get_int32("FD_HAND"));
    recv_resources->set_fd_sang(config.get_float("FD_SANG"));
    recv_resources->set_fd_mode(config.get_val("FD_MODE"));
    recv_resources->set_fa_req(config.get_float("FA_REQ"));
    recv_resources->set_nant(config.get_uint32("NANT"));
    recv_resources->set_antennas(config.get_val("ANTENNAE"));
    recv_resources->set_ant_weights(config.get_val("ANT_WEIGHTS"));
    recv_resources->set_npol(config.get_uint32("NPOL"));
    recv_resources->set_nbits(config.get_uint32("NBIT"));
    recv_resources->set_ndim(config.get_uint32("NDIM"));
    recv_resources->set_tsamp(config.get_double("TSAMP"));
    recv_resources->set_ovrsamp(config.get_val("OS_FACTOR"));
    recv_resources->set_nsubband(config.get_uint32("NSUBBAND"));
    recv_resources->set_udp_format(config.get_val("UDP_FORMAT"));
    recv_resources->set_bytes_per_second(config.get_double("BYTES_PER_SECOND"));
    recv_resources->set_beam_id(config.get_val("BEAM_ID"));
    subband_resources->set_data_key(config.get_val("DATA_KEY"));
    subband_resources->set_weights_key(config.get_val("WEIGHTS_KEY"));
    subband_resources->set_bandwidth(config.get_double("BW"));
    subband_resources->set_nchan(config.get_uint32("NCHAN"));
    subband_resources->set_start_channel(config.get_uint32("START_CHANNEL"));
    subband_resources->set_end_channel(config.get_uint32("END_CHANNEL"));
    subband_resources->set_frequency(config.get_double("FREQ"));
    subband_resources->set_start_channel_out(config.get_uint32("START_CHANNEL_OUT"));
    subband_resources->set_end_channel_out(config.get_uint32("END_CHANNEL_OUT"));
    subband_resources->set_nchan_out(config.get_uint32("NCHAN_OUT"));
    subband_resources->set_bandwidth_out(config.get_double("BW_OUT"));
    subband_resources->set_frequency_out(config.get_double("FREQ_OUT"));
    subband_resources->set_data_host(config.get_val("DATA_HOST"));
    subband_resources->set_data_port(config.get_uint32("DATA_PORT"));
}

void ska::pst::recv::RecvLmcServiceHandler::validate_scan_configuration(
    const ska::pst::lmc::ScanConfiguration& request
)
{
    auto config = scan_configuration_as_ascii_header(request);
    ska::pst::common::ValidationContext context;
    recv->validate_configure_scan(config, &context);
    context.throw_error_if_not_empty();
}


void ska::pst::recv::RecvLmcServiceHandler::configure_scan(
    const ska::pst::lmc::ScanConfiguration& request
)
{
    SPDLOG_TRACE("ska::pst::smrb::RecvLmcServiceHandler::configure_scan()");
    if (!is_beam_configured())
    {
        SPDLOG_WARN("Received scan configuration request when beam not configured already.");

        throw ska::pst::common::LmcServiceException(
            "RECV not configured for beam.",
            ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_BEAM,
            grpc::StatusCode::FAILED_PRECONDITION
        );
    }

    if (recv->is_scan_configured()) {
        SPDLOG_WARN("Received configure scan request but already configured.");

        throw ska::pst::common::LmcServiceException(
            "Scan already configured for RECV.",
            ska::pst::lmc::ErrorCode::CONFIGURED_FOR_SCAN_ALREADY,
            grpc::StatusCode::FAILED_PRECONDITION
        );
    }

    auto header = scan_configuration_as_ascii_header(request);
    // validation will happens on the state model as first part of configure_scan
    // so no need to do validation here.
    recv->configure_scan(header);
}

void ska::pst::recv::RecvLmcServiceHandler::deconfigure_scan()
{
    SPDLOG_TRACE("ska::pst::smrb::RecvLmcServiceHandler::deconfigure_scan()");
    if (!is_scan_configured())
    {
        SPDLOG_WARN("Received deconfigure when not already configured.");

        throw ska::pst::common::LmcServiceException(
            "RECV not configured for scan.",
            ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_SCAN,
            grpc::StatusCode::FAILED_PRECONDITION
        );
    }

    recv->deconfigure_scan();
}

void ska::pst::recv::RecvLmcServiceHandler::get_scan_configuration(
    ska::pst::lmc::ScanConfiguration* response
)
{
    if (!is_scan_configured())
    {
        SPDLOG_WARN("Received get_scan_configuration when not already configured.");

        throw ska::pst::common::LmcServiceException(
            "RECV not configured for scan.",
            ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_SCAN,
            grpc::StatusCode::FAILED_PRECONDITION
        );
    }

    auto receive_response = response->mutable_receive();
    auto &config = recv->get_scan_configuration();

    receive_response->set_activation_time(config.get_val("ACTIVATION_TIME"));
    receive_response->set_observer(config.get_val("OBSERVER"));
    receive_response->set_projid(config.get_val("PROJID"));
    receive_response->set_pnt_id(config.get_val("PNT_ID"));
    receive_response->set_subarray_id(config.get_val("SUBARRAY_ID"));
    receive_response->set_source(config.get_val("SOURCE"));
    receive_response->set_itrf(config.get_val("ITRF"));
    if (config.has("BMAJ"))
    {
        receive_response->set_bmaj(config.get_float("BMAJ"));
    }
    if (config.has("BMIN"))
    {
        receive_response->set_bmin(config.get_float("BMIN"));
    }
    receive_response->set_coord_md(config.get_val("COORD_MD"));
    receive_response->set_equinox(config.get_val("EQUINOX"));
    receive_response->set_stt_crd1(config.get_val("STT_CRD1"));
    receive_response->set_stt_crd2(config.get_val("STT_CRD2"));
    receive_response->set_trk_mode(config.get_val("TRK_MODE"));
    receive_response->set_scanlen_max(config.get_int32("SCANLEN_MAX"));
    if (config.has("TEST_VECTOR"))
    {
        receive_response->set_test_vector(config.get_val("TEST_VECTOR"));
    }
    receive_response->set_execution_block_id(config.get_val("EB_ID"));
}

void ska::pst::recv::RecvLmcServiceHandler::start_scan(
    const ska::pst::lmc::StartScanRequest& request
)
{
    SPDLOG_TRACE("ska::pst::recv::RecvLmcServiceHandler::scan()");
    if (!is_scan_configured())
    {
        SPDLOG_WARN("Received scan request when not already configured.");

        throw ska::pst::common::LmcServiceException(
            "RECV not configured for scan.",
            ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_SCAN,
            grpc::StatusCode::FAILED_PRECONDITION
        );
    }

    if (is_scanning())
    {
        SPDLOG_WARN("Received scan request when already.");

        throw ska::pst::common::LmcServiceException(
            "RECV is already scanning.",
            ska::pst::lmc::ErrorCode::ALREADY_SCANNING,
            grpc::StatusCode::FAILED_PRECONDITION
        );
    }

    // add the scan_id here
    ska::pst::common::AsciiHeader start_scan_config;
    start_scan_config.set("SCAN_ID", request.scan_id());
    recv->start_scan(start_scan_config);
}

void ska::pst::recv::RecvLmcServiceHandler::stop_scan()
{
    SPDLOG_TRACE("ska::pst::recv::RecvLmcServiceHandler::stop_scan()");
    if (!is_scanning())
    {
        SPDLOG_WARN("Received stop_scan request when not scanning.");

        throw ska::pst::common::LmcServiceException(
            "Received stop_scan request when RECV is not scanning.",
            ska::pst::lmc::ErrorCode::NOT_SCANNING,
            grpc::StatusCode::FAILED_PRECONDITION
        );
    }

    recv->stop_scan();
}

void ska::pst::recv::RecvLmcServiceHandler::reset()
{
    SPDLOG_INFO("ska::pst::recv::RecvLmcServiceHandler::reset()");
    if (recv->get_state() == ska::pst::common::State::RuntimeError) {
        recv->reset();
    }
}

void ska::pst::recv::RecvLmcServiceHandler::get_monitor_data(
    ska::pst::lmc::MonitorData *response
)
{
    SPDLOG_TRACE("ska::pst::recv::RecvLmcServiceHandler::get_monitor_data()");
    if (!is_scanning())
    {
        SPDLOG_WARN("Received get_monitor_data request when not scanning.");

        throw ska::pst::common::LmcServiceException(
            "Received get_monitor_data request when RECV is not scanning.",
            ska::pst::lmc::ErrorCode::NOT_SCANNING,
            grpc::StatusCode::FAILED_PRECONDITION
        );
    }

    const auto &stats = recv->get_stats();

    auto *receive_monitor_data = response->mutable_receive();
    receive_monitor_data->set_receive_rate(static_cast<float>(stats.get_data_transmission_rate()));
    receive_monitor_data->set_data_received(stats.get_data_transmitted());
    receive_monitor_data->set_data_drop_rate(static_cast<float>(stats.get_data_drop_rate()));
    receive_monitor_data->set_data_dropped(stats.get_data_dropped());
    receive_monitor_data->set_malformed_packets(stats.get_malformed());
    receive_monitor_data->set_misdirected_packets(stats.get_misdirected());
    receive_monitor_data->set_misordered_packets(stats.get_misordered());
    SPDLOG_DEBUG("ska::pst::recv::RecvLmcServiceHandler::get_monitor_data malformed={}", stats.get_malformed());
    SPDLOG_DEBUG("ska::pst::recv::RecvLmcServiceHandler::get_monitor_data misdirected={}", stats.get_misdirected());
    SPDLOG_DEBUG("ska::pst::recv::RecvLmcServiceHandler::get_monitor_data misordered={}", stats.get_misordered());
    SPDLOG_DEBUG("ska::pst::recv::RecvLmcServiceHandler::get_monitor_data data_transmitted={}", stats.get_data_transmitted());
}

void ska::pst::recv::RecvLmcServiceHandler::get_env(
    ska::pst::lmc::GetEnvironmentResponse *response
) noexcept
{
    SPDLOG_TRACE("ska::pst::recv::RecvLmcServiceHandler::get_env()");
    auto values = response->mutable_values();

    ska::pst::lmc::EnvValue data_host;
    data_host.set_string_value(recv->get_data_host());
    (*values)["data_host"] = data_host;

    ska::pst::lmc::EnvValue data_port;
    data_port.set_signed_int_value(recv->get_data_port());
    (*values)["data_port"] = data_port;

    SPDLOG_TRACE("ska::pst::recv::RecvLmcServiceHandler::get_env returning: {}", response->ShortDebugString());
}

void ska::pst::recv::RecvLmcServiceHandler::go_to_runtime_error(
    std::exception_ptr exc
)
{
    SPDLOG_TRACE("ska::pst::recv::RecvLmcServiceHandler::go_to_runtime_error()");
    recv->go_to_runtime_error(exc);
}
