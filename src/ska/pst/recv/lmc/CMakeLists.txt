set(public_headers)

set(sources
    src/RecvLmcServiceHandler.cpp
)

set(private_headers
    RecvLmcServiceHandler.h
)

add_library(
    ska-pst-recv-lmc
    OBJECT
    ${sources} ${optional_sources}
    ${private_headers}
    ${public_headers} ${optional_public_headers}
)

target_include_directories(
    ska-pst-recv-lmc
    PUBLIC
    $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/src>
    $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}/src>
    $<INSTALL_INTERFACE:include>
    ${SkaPstSmrb_INCLUDE_DIR}
    ${PSRDADA_INCLUDE_DIR}
)

target_link_libraries(
    ska-pst-recv-lmc
    PUBLIC
    spdlog::spdlog
    protobuf::libprotobuf
    gRPC::grpc
    gRPC::grpc++
    ${SKAPSTLMC_LIBRARIES}
)

if (BUILD_TESTING)
    add_subdirectory(tests)
endif()
