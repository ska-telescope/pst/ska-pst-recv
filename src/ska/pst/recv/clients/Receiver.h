/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include <vector>
#include <map>
#include <cstdlib>
#include <string>
#include <thread>
#include <mutex>
#include <condition_variable>

#include "ska/pst/common/utils/AsciiHeader.h"
#include "ska/pst/common/utils/ValidationContext.h"
#include "ska/pst/common/statemodel/StateModelException.h"
#include "ska/pst/common/statemodel/ApplicationManager.h"

#include "ska/pst/recv/network/SocketReceive.h"
#include "ska/pst/recv/network/UDPStats.h"

#ifndef SKA_PST_RECV_Receiver_h
#define SKA_PST_RECV_Receiver_h

#define INIT_DB_ACCOUNTING_T {nullptr, nullptr, 0, 0, 0, 0, 0, 0}

namespace ska::pst::recv {

  /**
   * @brief manage the pointers, buffer sizes and byte offsets for the
   * 2 buffers used in the Receiver algorithms.
   *
   */
  typedef struct db_accounting {

    //! Pointer to the current data buffer to which data are written
    char * curr_buf;

    //! Pointer to the next data buffer to which data are written
    char * next_buf;

    //! Size of the curr and next buffers
    uint64_t bufsz;

    //! Byte offset from the start of the observation of the first byte in the curr_buf
    uint64_t curr_byte_offset;

    //! Byte offset from the start of the observation of the first byte in the next_buf
    uint64_t next_byte_offset;

    //! Byte offset from the start of the observation of the last byte in the next_buf
    uint64_t last_byte_offset;

    //! Number of bytes written to the curr_buf
    uint64_t bytes_curr_buf;

    //! Number of bytes written to the next buf
    uint64_t bytes_next_buf;

  } db_accounting_t;

  /**
   * @brief Abstract base class that provides methods for receiving data streams
   *
   */
  class Receiver : public ska::pst::common::ApplicationManager {

    public:

      /**
       * @brief Construct a new Receiver object that is configured to receive data at the host:port UDP endpoint
       *
       * @param host IPv4 address on which to receive UDP data streams
       * @param port UDP port on which to receive UDP data streams
       */
      Receiver(std::string host, int port);

      /**
       * @brief Construct a new Receiver object that is configured to receive data on the specified IPv4 address.
       * Note that the port on which to receive the data streams must then be provided in the beam configuration.
       *
       * @param host IPv4 address on which to receive UDP data streams
       */
      Receiver(std::string host);

      /**
       * @brief Destroy the Receiver object
       *
       */
      ~Receiver();

      /**
       * @brief Validate requested beam configuration
       *
       * @param config AsciiHeader containing Beam Configuration
       * @param context A validation context where errors should be added.
       */
      void validate_configure_beam(const ska::pst::common::AsciiHeader& config, ska::pst::common::ValidationContext *context) override;

      /**
       * @brief Validate requested scan configuration
       *
       * @param config AsciiHeader containing Scan Configuration
       * @param context A validation context where errors should be added.
       */
      void validate_configure_scan(const ska::pst::common::AsciiHeader& config, ska::pst::common::ValidationContext *context) override;

      /**
       * @brief Validate requested start scan configuration
       *
       * @param config AsciiHeader containing Start Scan Configuration
       */
      void validate_start_scan(const ska::pst::common::AsciiHeader& config) override;

      /**
       * @brief Initialisation callback.
       *
       */
      void perform_initialise() override;

      /**
       * @brief Signal to start data acqusition
       *
       */
      void start_receiving();

      /**
       * @brief Signal to stop data acquisition
       *
       */
      void stop_receiving();


      /**
       * @brief Parse the configuration provided to the Receiver in the configure_beam method
       * Raises a runtime_error if configuration is invalid.
       */
      void parse_beam_config();

      /**
       * @brief Parse the header provided to the Receiver in the configurat_scan method
       * Raises a runtime_error if the header is invalid.
       */
      void parse_scan_config();

      /**
       * @brief Monitor the data receive performance whilst the control_state is Recording
       *
       */
      void monitor_method();

      /**
       * @brief StopScan callback that is called by \ref ska::pst::common::ApplicationManager::main.
       * Contains the instructions required prior to transitioning the state from Scanning to ScanConfigured.
       *
       */
      void perform_stop_scan();

      /**
       * @brief Print scan statistics by printing values from UDPStats
       *
       */
      void print_stats();

      //! Validates existence of configuration keys. Check found key value patterns. Throws all missing keys and non conforming value patterns
      void check_config_value_patterns(const std::string& config_name, const ska::pst::common::AsciiHeader& config, const std::map<std::string,std::string>& config_value_patterns, ska::pst::common::ValidationContext *context);

      /**
       * @brief Return the size of the data buffer that is receiving the streams.
       *
       * @return uint64_t size of the data buffer in bytes
       */
      virtual uint64_t get_data_bufsz() = 0;

      /**
       * @brief Get a const reference of the stats for the Receiver
       *
       */
      virtual const UDPStats& get_stats() { return stats; }

      /**
       * @brief Get the scan_id of the Receiver.
       *
       * @return uint64_t scan_id
       */
      uint64_t get_scan_id() const { return scan_id; }

      /**
       * @brief Get the configured data_host.
       */
      virtual const std::string get_data_host() { return data_host; }

      /**
       * @brief Get the configured data_port.
       */
      virtual const int get_data_port() { return data_port; }

      /**
       * @brief Reset the accounting metadata, configure the curr, next and last byte
       * offsets to their default values and resetting the number bytes in the curr
       * and next buffers.
       *
       * @param acc accounting metadata structure to reset.
       */
      static void reset_accounting_meta(db_accounting_t * acc)
      {
        acc->curr_byte_offset = 0;
        acc->next_byte_offset = acc->bufsz;
        acc->last_byte_offset = acc->next_byte_offset + acc->bufsz;
        acc->bytes_curr_buf = 0;
        acc->bytes_next_buf = 0;
      }

      /**
       * @brief Rotate the acounting metadata, incrementing the curr, next and last byte offsets
       * by the bufsz. Rotates the bytes_next_buf to bytes_curr_buf and resets bytes_next_buf.
       *
       * @param acc accounting metadata structure to rotate.
       */
      static void rotate_accounting_meta(db_accounting_t * acc)
      {
        acc->curr_byte_offset = acc->next_byte_offset;
        acc->next_byte_offset = acc->last_byte_offset;
        acc->last_byte_offset += acc->bufsz;

        // account for data saved into the next buf
        acc->bytes_curr_buf = acc->bytes_next_buf;
        acc->bytes_next_buf = 0;
      }

      /**
       * @brief Rotate the accounting data pointers, switching the curr and next buffers.
       *
       * @param acc accounting data to rotate
       */
      static void rotate_accounting_buffers(db_accounting_t * acc)
      {
        char * tmp_buf = acc->curr_buf;
        acc->curr_buf = acc->next_buf;
        acc->next_buf = tmp_buf;
      }

      /**
       * @brief Advance the accounting data points, settin the curr buffer to the next buffer
       * and resetting the next buffer to nullptr.
       *
       * @param acc
       */
      static void advance_accounting_buffers(db_accounting_t * acc)
      {
        acc->curr_buf = acc->next_buf;
        acc->next_buf = nullptr;
      }

    protected:

      /**
       * @brief IPv4 network address the Receiver will be acquiring UDP data on.
       *
       */
      std::string data_host{};

      /**
       * @brief UDP port number the Receiver will be acquiring data on.
       *
       */
      int data_port{0};

      /**
       * @brief List of UDPStats objects, one for each source.
       * These classes record the UDP acquisition statistics for each source
       */
      UDPStats stats;

      /**
       * @brief Number of distinct signals present in the input data streams.
       * This number should always be configured as 1.
       */
      unsigned nsig{0};

      /**
       * @brief Number of channels in the input data streams.
       *
       */
      unsigned nchan{0};

      /**
       * @brief Number of bits per sample in the input data streams.
       * The number of bits corresponds to each datum. So a complex sample (8b real + 8b imag) would be represented as nbit=8.
       */
      unsigned nbit{0};

      /**
       * @brief Number of polarisations in the input data streams.
       *
       */
      unsigned npol{0};

      /**
       * @brief Number of dimesions in the input data streams.
       * Real data ndim=1, Complex data: ndim=2
       */
      unsigned ndim{0};

      /**
       * @brief Sampling interval of the input data streams in units of microseconds.
       *
       */
      double tsamp{0};

      /**
       * @brief Critical bandwdith of the input data streams in units of MegaHertz.
       *
       */
      double bw{0};

      /**
       * @brief Centre frequency of the input data streams in uints of Megahertz.
       *
       */
      double freq{0};

      /**
       * @brief First input channel to be acquired.
       *
       */
      unsigned start_channel{0};

      /**
       * @brief Last input channel to be acquired.
       *
       */
      unsigned end_channel{0};

      /**
       * @brief Number of bits per second in the input data streams.
       *
       */
      double bits_per_second{0};

      /**
       * @brief Number of bytes per second in the inputs data streams.
       *
       */
      double bytes_per_second{0};

      /**
       * @brief Delay, in seconds, to apply in the Receiver before data acqusition commences
       *
       */
      unsigned start_delay{2};

      /**
       * @brief Start time of the data stream
       *
       */
      ska::pst::common::Time utc_start{};

      /**
       * @brief The SCAN ID for this data stream
       *
       */
      uint64_t scan_id{UINT64_MAX};

      /**
       * @brief pointers and offsets for the data output
       *
       */
      db_accounting_t data{};

      /**
       * @brief pointers and offsets for the weights output
       *
       */
      db_accounting_t weights{};

      /**
       * @brief time out to use during beam configuration
       *
       */
      int timeout{0};

      void set_config_value_patterns(std::map<std::string,std::string> beam_config, std::map<std::string,std::string> scan_config, std::map<std::string,std::string> start_scan_config);

      std::map<std::string,std::string> beam_config_value_patterns{
        { "", "" }
      };

      std::map<std::string,std::string> scan_config_value_patterns{
        { "", "" }
      };

      std::map<std::string,std::string> start_scan_config_value_patterns{
        { "", "" }
      };

      /**
       * @brief Thread identifier for the monitoring thread.
       *
       */
      std::unique_ptr<std::thread> monitor_thread{nullptr};

      bool keep_receiving{false};

    private:
  };

} // namespace ska::pst::recv

#endif
