/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <spdlog/spdlog.h>
#include <thread>

#include "ska/pst/recv/formats/UDPFormatFactory.h"
#include "ska/pst/recv/testutils/GtestMain.h"
#include "ska/pst/recv/clients/tests/UDPGeneratorTest.h"
#include "ska/pst/recv/network/UDPSocketReceive.h"
#include "ska/pst/recv/clients/UDPReceiver.h"

auto main(int argc, char* argv[]) -> int
{
  return ska::pst::recv::test::gtest_main(argc, argv);
}

namespace ska::pst::recv::test {

static constexpr unsigned tobs = 1; // second
static constexpr float default_data_rate = 12500000.0; // 0.1 Gbps or 2.5 MiB/s
static constexpr float compute_data_rate = -1;

UDPGeneratorTest::UDPGeneratorTest()
    : ::testing::Test()
{
}

void UDPGeneratorTest::SetUp()
{
  beam_config.load_from_file(test_data_file("complete_config.txt"));
  scan_config.load_from_file(test_data_file("complete_config.txt"));
  startscan_config.set_val("SCAN_ID", "1");
}

void UDPGeneratorTest::TearDown()
{
}

void UDPGeneratorTest::stop_transmit(UDPGenerator & udpgen, unsigned delay_us)
{
  usleep(delay_us);
  udpgen.stop_transmit();
}

TEST_F(UDPGeneratorTest, test_default_constructor) // NOLINT
{
  UDPGenerator udpgen;
}

TEST_F(UDPGeneratorTest, test_configure_beam) // NOLINT
{
  UDPGenerator udpgen;
  udpgen.configure_beam(beam_config);
}

TEST_F(UDPGeneratorTest, test_transmit) // NOLINT
{
  UDPGenerator udpgen;
  udpgen.configure_beam(beam_config);
  udpgen.configure_scan(scan_config);
  udpgen.transmit(tobs, default_data_rate);
}

TEST_F(UDPGeneratorTest, test_transmit_no_data_rate) // NOLINT
{
  UDPGenerator udpgen;
  udpgen.configure_beam(beam_config);
  udpgen.configure_scan(scan_config);
  static constexpr float data_rate = -1;
  udpgen.transmit(tobs, data_rate);
}

TEST_F(UDPGeneratorTest, test_transmit_no_utc_start) // NOLINT
{
  UDPGenerator udpgen;
  udpgen.configure_beam(beam_config);

  ska::pst::common::AsciiHeader alt_header;
  alt_header.clone(scan_config);
  alt_header.del("UTC_START");

  udpgen.configure_scan(alt_header);

  udpgen.transmit(tobs, default_data_rate);
}

TEST_F(UDPGeneratorTest, test_transmit_no_source) // NOLINT
{
  UDPGenerator udpgen;
  udpgen.configure_beam(beam_config);

  ska::pst::common::AsciiHeader alt_header;
  alt_header.clone(scan_config);

  alt_header.del("SOURCE");
  EXPECT_THROW(udpgen.configure_scan(alt_header), std::runtime_error); // NOLINT
}

TEST_F(UDPGeneratorTest, test_bad_data_generator) // NOLINT
{
  UDPGenerator udpgen;
  udpgen.configure_beam(beam_config);

  ska::pst::common::AsciiHeader alt_header;
  alt_header.clone(scan_config);

  alt_header.set("DATA_GENERATOR", "Garbage");
  EXPECT_THROW(udpgen.configure_scan(alt_header), std::runtime_error); // NOLINT
}

TEST_F(UDPGeneratorTest, test_transmit_random_generator) // NOLINT
{
  UDPGenerator udpgen;
  udpgen.configure_beam(beam_config);

  ska::pst::common::AsciiHeader alt_header;
  alt_header.clone(scan_config);

  alt_header.set("DATA_GENERATOR", "Random");
  udpgen.configure_scan(alt_header);
  ska::pst::recv::UDPStats& stats = udpgen.get_stats();

  ASSERT_EQ(stats.get_data_transmitted(), 0);
  ASSERT_EQ(stats.get_data_dropped(), 0);
  ASSERT_EQ(stats.get_misordered(), 0);
  ASSERT_EQ(stats.get_malformed(), 0);
  ASSERT_EQ(stats.get_discarded(), 0);

  udpgen.transmit(tobs, default_data_rate);

  ASSERT_GT(stats.get_data_transmitted(), 0);
  ASSERT_EQ(stats.get_data_dropped(), 0);
  ASSERT_EQ(stats.get_misordered(), 0);
  ASSERT_EQ(stats.get_malformed(), 0);
  ASSERT_EQ(stats.get_discarded(), 0);
}

TEST_F(UDPGeneratorTest, test_transmit_sine_generator) // NOLINT
{
  UDPGenerator udpgen;
  udpgen.configure_beam(beam_config);

  ska::pst::common::AsciiHeader alt_header;
  alt_header.clone(scan_config);

  double cfreq = beam_config.get_double("FREQ");
  double bw = beam_config.get_double("FREQ");
  double sinusoid_freq = cfreq + (bw / 4);

  alt_header.set("DATA_GENERATOR", "Sine");
  alt_header.set("SINUSOID_FREQ", sinusoid_freq);
  udpgen.configure_scan(alt_header);

  ska::pst::recv::UDPStats& stats = udpgen.get_stats();

  ASSERT_EQ(stats.get_data_transmitted(), 0);
  ASSERT_EQ(stats.get_data_dropped(), 0);
  ASSERT_EQ(stats.get_misordered(), 0);
  ASSERT_EQ(stats.get_malformed(), 0);
  ASSERT_EQ(stats.get_discarded(), 0);

  udpgen.transmit(tobs, default_data_rate);

  ASSERT_GT(stats.get_data_transmitted(), 0);
  ASSERT_EQ(stats.get_data_dropped(), 0);
  ASSERT_EQ(stats.get_misordered(), 0);
  ASSERT_EQ(stats.get_malformed(), 0);
  ASSERT_EQ(stats.get_discarded(), 0);
}

TEST_F(UDPGeneratorTest, test_transmit_gaussian_noise_generator) // NOLINT
{
  UDPGenerator udpgen;
  udpgen.configure_beam(beam_config);

  ska::pst::common::AsciiHeader alt_header;
  alt_header.clone(scan_config);

  alt_header.set("DATA_GENERATOR", "GaussianNoise");
  alt_header.set("NORMAL_DIST_MEAN", "0.0");
  alt_header.set("NORMAL_DIST_STDDEV", "10.0");
  alt_header.set("NORMAL_DIST_RED_STDDEV", "1.0");
  udpgen.configure_scan(alt_header);

  ska::pst::recv::UDPStats& stats = udpgen.get_stats();

  ASSERT_EQ(stats.get_data_transmitted(), 0);
  ASSERT_EQ(stats.get_data_dropped(), 0);
  ASSERT_EQ(stats.get_misordered(), 0);
  ASSERT_EQ(stats.get_malformed(), 0);
  ASSERT_EQ(stats.get_discarded(), 0);

  udpgen.transmit(tobs, default_data_rate);

  ASSERT_GT(stats.get_data_transmitted(), 0);
  ASSERT_EQ(stats.get_data_dropped(), 0);
  ASSERT_EQ(stats.get_misordered(), 0);
  ASSERT_EQ(stats.get_malformed(), 0);
  ASSERT_EQ(stats.get_discarded(), 0);
}

TEST_F(UDPGeneratorTest, test_transmit_sqaure_wave_generator) // NOLINT
{
  UDPGenerator udpgen;
  udpgen.configure_beam(beam_config);

  ska::pst::common::AsciiHeader alt_header;
  alt_header.clone(scan_config);

  alt_header.set("DATA_GENERATOR", "SquareWave");
  alt_header.set("CAL_OFF_INTENSITY", "10");
  alt_header.set("CAL_ON_POL_0_INTENSITY", "11");
  alt_header.set("CAL_ON_POL_1_CHAN_0_INTENSITY", "9");
  alt_header.set("CAL_ON_POL_1_CHAN_N_INTENSITY", "12");
  udpgen.configure_scan(alt_header);

  ska::pst::recv::UDPStats& stats = udpgen.get_stats();

  ASSERT_EQ(stats.get_data_transmitted(), 0);
  ASSERT_EQ(stats.get_data_dropped(), 0);
  ASSERT_EQ(stats.get_misordered(), 0);
  ASSERT_EQ(stats.get_malformed(), 0);
  ASSERT_EQ(stats.get_discarded(), 0);

  udpgen.transmit(tobs, default_data_rate);

  ASSERT_GT(stats.get_data_transmitted(), 0);
  ASSERT_EQ(stats.get_data_dropped(), 0);
  ASSERT_EQ(stats.get_misordered(), 0);
  ASSERT_EQ(stats.get_malformed(), 0);
  ASSERT_EQ(stats.get_discarded(), 0);
}

TEST_F(UDPGeneratorTest, test_transmit_stop_transmit) // NOLINT
{
  UDPGenerator udpgen;
  udpgen.configure_beam(beam_config);
  udpgen.configure_scan(scan_config);
  static constexpr unsigned delay_us = 500000;

  std::thread delay_thread = std::thread(&UDPGeneratorTest::stop_transmit, this, std::ref(udpgen), delay_us);
  udpgen.transmit(tobs, compute_data_rate);
  delay_thread.join();
}

TEST_F(UDPGeneratorTest, test_transmit_invalid_packets) // NOLINT
{
  // disabling due to poor network performance on AWS
  GTEST_SKIP();

  // setup a UDP receiver
  std::shared_ptr<SocketReceive> sock = std::make_shared<UDPSocketReceive>();
  std::unique_ptr<UDPReceiver> recv = std::make_unique<UDPReceiver>(sock, beam_config.get_val("DATA_HOST"));
  recv->configure_beam(beam_config);
  recv->configure_scan(scan_config);
  recv->start_scan(startscan_config);

  // start at a high packet sequence number to support the MisorderedPacketSequenceNumber test
  static constexpr uint64_t start_psn = 1000;
  UDPGenerator udpgen;
  udpgen.configure_beam(beam_config);
  udpgen.configure_scan(scan_config);
  udpgen.set_start_packet_sequence_number(start_psn);

  uint64_t expected_malformed{0}, expected_dropped{0}, expected_misdirected{0};

  // beam configuration dictates 18 packets per frame
  udpgen.add_induced_error(1, ska::pst::recv::FailureType::BadMagicWord); // NOLINT
  expected_malformed++;
  expected_dropped++;

  udpgen.add_induced_error(8, ska::pst::recv::FailureType::BadPacketSize); // NOLINT
  expected_malformed++;
  expected_dropped++;

  udpgen.add_induced_error(12, ska::pst::recv::FailureType::BadTransmit); // NOLINT
  expected_dropped++;

  udpgen.add_induced_error(16, ska::pst::recv::FailureType::MisorderedPacketSequenceNumber); // NOLINT
  expected_dropped++;

  // these errors are not detectable yet, just check that they can be generated
  udpgen.add_induced_error(18, ska::pst::recv::FailureType::BadScanID); // NOLINT
  expected_misdirected++;
  expected_dropped++;

  udpgen.add_induced_error(21, ska::pst::recv::FailureType::BadChannelNumber); // NOLINT
  expected_misdirected++;
  expected_dropped++;

  udpgen.add_induced_error(23, ska::pst::recv::FailureType::BadTimestamp); // NOLINT
  udpgen.add_induced_error(25, ska::pst::recv::FailureType::BadDataRate); // NOLINT

  SPDLOG_INFO("");
  udpgen.transmit(tobs+10, default_data_rate);

  const ska::pst::recv::UDPStats &stats = recv->get_stats();
  EXPECT_EQ(stats.get_malformed(), expected_malformed);
  EXPECT_EQ(stats.get_packets_dropped(), expected_dropped);
  EXPECT_EQ(stats.get_misdirected(), expected_misdirected);

  recv->stop_scan();
  recv->deconfigure_scan();
  recv->deconfigure_beam();
}

} // namespace ska::pst::recv::test
