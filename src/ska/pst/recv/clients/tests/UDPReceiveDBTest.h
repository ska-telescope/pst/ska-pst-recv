/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <gtest/gtest.h>
#include <string>
#include <memory>

#include "ska/pst/recv/clients/UDPReceiveDB.h"
#include "ska/pst/recv/network/UDPSocketReceive.h"
#include "ska/pst/smrb/DataBlockCreate.h"
#include "ska/pst/smrb/DataBlockWrite.h"

#ifndef SKA_PST_RECV_TESTS_UDPReceiveDBTest_h
#define SKA_PST_RECV_TESTS_UDPReceiveDBTest_h

namespace ska::pst::recv::test {

class TestUDPReceiveDB : public UDPReceiveDB
{
    public:
        TestUDPReceiveDB(std::shared_ptr<SocketReceive> sock, const std::string& host) : UDPReceiveDB(sock, host) {};
        ~TestUDPReceiveDB() = default;

        // Resources
        uint64_t get_stats_data_transmitted() { return stats.get_data_transmitted(); };
        uint64_t get_stats_data_transmission_rate() { return stats.get_data_transmission_rate(); };
        uint64_t get_stats_ignored() { return stats.get_ignored(); };
        uint64_t get_stats_malformed() { return stats.get_malformed(); };
        uint64_t get_stats_misdirected() { return stats.get_misdirected(); };
        uint64_t get_stats_packets_dropped() { return stats.get_packets_dropped(); };
        uint64_t get_stats_packet_receive_timeouts() { return stats.get_packet_receive_timeouts(); };
        bool     check_running_monitor_thread() { return monitor_thread->joinable(); };
};

/**
 * @brief Test the UDPReceiveDB class
 *
 * @details
 *
 */
class UDPReceiveDBTest : public ::testing::Test
{
  protected:

    void SetUp() override;

    void TearDown() override;

    std::shared_ptr<UDPSocketReceive> sock{nullptr};

    std::shared_ptr<TestUDPReceiveDB> recv{nullptr};

    std::shared_ptr<UDPFormat> format{nullptr};

    ska::pst::common::AsciiHeader beam_config;

    ska::pst::common::AsciiHeader scan_config;

    ska::pst::common::AsciiHeader startscan_config;

    std::string db_key;

    std::string wb_key;

    ska::pst::smrb::DataBlockCreate db;

    ska::pst::smrb::DataBlockCreate wb;

    uint64_t _scan_id{0};

  public:

    UDPReceiveDBTest() ;

    ~UDPReceiveDBTest() = default;

    void set_config();

    uint64_t generate_scan_id();

    void reader(const std::string &key, uint64_t bufsz, bool validate_data, bool* valid_data_sequence);
    unsigned number_of_scans{1};

    uint64_t header_nbufs{4};

    uint64_t header_bufsz{4096};

    uint64_t data_nbufs{4};

    uint64_t data_resolution{0};

    uint64_t weights_nbufs{4};

    uint64_t weights_resolution{0};

  private:

};

} // namespace ska::pst::recv::test

#endif // SKA_PST_RECV_TESTS_UDPReceiveDBTest_h

