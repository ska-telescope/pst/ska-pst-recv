/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <cstdlib>

#include "ska/pst/common/utils/AsciiHeader.h"
#include "ska/pst/common/utils/PacketGenerator.h"
#include "ska/pst/recv/formats/UDPFormat.h"
#include "ska/pst/recv/network/SocketReceive.h"
#include "ska/pst/recv/clients/Receiver.h"

#ifndef SKA_PST_RECV_NETWORK_UDPReceiver_h
#define SKA_PST_RECV_NETWORK_UDPReceiver_h

namespace ska::pst::recv {

  /**
   * @brief Concrete implementation of a Receiver that receives a UDP data stream.
   * The data stream is defined by the UDPFormat and is written into local buffers
   * to simulate data acqusition requiring real memory bandwidth.
   */
  class UDPReceiver : public Receiver {

    public:

      /**
       * @brief Construct a new UDPReceiver object using provided socket to receive data from the 
       * UDP endpoint at host
       *
       * @param sock SocketReceive on which the data will be received
       * @param host IPv4 address on the sock will used
       */
      UDPReceiver(std::shared_ptr<SocketReceive> sock, const std::string& host);

      /**
       * @brief Construct a new UDPReceiver object using provided socket to receive data from the 
       * UDP endpoint at host:port
       * 
       * @param sock SocketReceive on which the data will be received
       * @param host IPv4 address on the sock will used
       * @param port UDP port on which the sock will receive data
       */
      UDPReceiver(std::shared_ptr<SocketReceive> sock, const std::string& host, int port);

      /**
       * @brief Destroy the UDPReceiver object
       *
       */
      ~UDPReceiver();

      /**
       * @brief Process fixed configuration and perform beam resource allocation.
       * Configures the receiver using the fixed configuration parameters that are
       * common to all observations and available at launch. Configures the UDPFormat
       * and UDPStats instances. Opens the UDP socket and allocates memory buffers
       * to receive UDP packets.
       *
       */
      void perform_configure_beam();

      /**
       * @brief Release beam resources of the Receiver, which deallocates the receive
       * socket and release the buffer memory.
       *
       */
      void perform_deconfigure_beam();

      /**
       * @brief Processes the runtime configuration parameters in the header.
       * Prepares the format with the runtime configuration parameters and starts
       * the monitoring thread.
       */
      void perform_configure_scan();

      /**
       * @brief Conclude the format and reset the header and stats.
       *
       */
      void perform_deconfigure_scan();

      /**
       * @brief Scan callback that is called by \ref ska::pst::common::ApplicationManager::main.
       * Contains scanning instructions meant to be launched in a separate thread.
       *
       */
      void perform_scan();

      /**
       * @brief StartScan callback that is called by \ref ska::pst::common::ApplicationManager::main.
       * Contains the instructions required prior to transitioning the state from ScanConfigured to Scanning.
       * Launches perform_scan on a separate thread.
       *
       */
      void perform_start_scan();
      
      /**
       * @brief Terminate callback that is called by \ref ka::pst::common::ApplicationManager::main.
       * Contains the instructions required prior to transitioning the state from RuntimeError to Idle.
       *
       */
      void perform_terminate();

      /**
       * @brief Receive a single block of data
       *
       */
      void receive_block();

      /**
       * @brief Return the size of the data buffer.
       *
       * @return uint64_t size of the data buffer in bytes.
       */
      uint64_t get_data_bufsz() override;

    protected:

      //! UDP format provides the mapping from UDP metadata to memory addresses
      std::shared_ptr<UDPFormat> format{nullptr};

      //! UDP receiving socket
      std::shared_ptr<SocketReceive> socket{nullptr};

      //! flag for presence of UTC_START
      bool have_utc_start{false};

      //! Optional PacketGenerator validates the data and weights of incoming stream
      std::shared_ptr<common::PacketGenerator> data_generator{nullptr};

    private:

      //! first receiver buffer
      std::vector<char> _data_buffer_a;

      //! second receiver buffer
      std::vector<char> _data_buffer_b;

      //! first receiver buffer
      std::vector<char> _weights_buffer_a;

      //! second receiver buffer
      std::vector<char> _weights_buffer_b;

  };

} // namespace ska::pst::recv

#endif // SKA_PST_RECV_NETWORK_UDPReceiver_h
