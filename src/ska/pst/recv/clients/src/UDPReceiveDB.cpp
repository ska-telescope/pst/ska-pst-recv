/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <unistd.h>
#include <stdexcept>
#include <string>
#include <spdlog/spdlog.h>

#include "ska/pst/recv/definitions.h"
#include "ska/pst/recv/clients/UDPReceiveDB.h"
#include "ska/pst/recv/formats/UDPFormatFactory.h"

ska::pst::recv::UDPReceiveDB::UDPReceiveDB(std::shared_ptr<ska::pst::recv::SocketReceive> sock, const std::string& host)
   : ska::pst::recv::Receiver(host),
  socket(std::move(sock))
{
  SPDLOG_TRACE("pst::recv::UDPReceiveDB::UDPReceiveDB constructor with host={}", host);
  beam_config_value_patterns = {
      { "DATA_HOST", "^(([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5]).){3}([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])$" },
      { "DATA_KEY", "^[a-f0-9]{4,}$" },
      { "WEIGHTS_KEY", "^[a-f0-9]{4,}$" },
      { "UDP_FORMAT", "^[A-Za-z]{1,}$" },
      { "OS_FACTOR", "^[0-9]{1,}/[0-9]{1,}$" },
      { "NCHAN", "^[0-9]{1,}$" },
      { "NBIT", "^[0-9]{1,}$" },
      { "NPOL", "^[0-9]{1,}$" },
      { "NDIM", "^[0-9]{1,}$" },
      { "TSAMP", "^[0-9]{1,}.[0-9]{1,}$" },
      { "BW", "^[0-9]{1,}.[0-9]{1,}$" },
      { "FREQ", "^([0-9]{1,}|[0-9]{1,}.[0-9]{1,})$" },
      { "START_CHANNEL", "^[0-9]{1,}$" },
      { "END_CHANNEL", "^[0-9]{1,}$" },
      { "BEAM_ID", "^[0-9]{1,}$" }
  };
  scan_config_value_patterns = {
      // allow any alphanumeric, _, +, or - chars.
      { "SOURCE", "^[\\w+-]+$" },
      // Regex comes from SKA Telemodel example eb-m001-20230921-12345.
      { "EB_ID", "^eb\\-[a-z0-9]+\\-[0-9]{8}\\-[a-z0-9]+$" } // NOLINT
  };
  start_scan_config_value_patterns = {
      { "SCAN_ID", "^[0-9]{1,}$" }
  };
}

ska::pst::recv::UDPReceiveDB::UDPReceiveDB(std::shared_ptr<ska::pst::recv::SocketReceive> sock, const std::string& host, int port)
   : ska::pst::recv::Receiver(host, port),
  socket(std::move(sock))
{
  SPDLOG_TRACE("pst::recv::UDPReceiveDB::UDPReceiveDB constructor with host={} port={}", host, port);
  beam_config_value_patterns = {
      { "DATA_HOST", "^(([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5]).){3}([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])$" },
      { "DATA_KEY", "^[a-f0-9]{4,}$" },
      { "WEIGHTS_KEY", "^[a-f0-9]{4,}$" },
      { "UDP_FORMAT", "^[A-Za-z]{1,}$" },
      { "OS_FACTOR", "^[0-9]{1,}/[0-9]{1,}$" },
      { "NCHAN", "^[0-9]{1,}$" },
      { "NBIT", "^[0-9]{1,}$" },
      { "NPOL", "^[0-9]{1,}$" },
      { "NDIM", "^[0-9]{1,}$" },
      { "TSAMP", "^[0-9]{1,}.[0-9]{1,}$" },
      { "BW", "^[0-9]{1,}.[0-9]{1,}$" },
      { "FREQ", "^([0-9]{1,}|[0-9]{1,}.[0-9]{1,})$" },
      { "START_CHANNEL", "^[0-9]{1,}$" },
      { "END_CHANNEL", "^[0-9]{1,}$" },
      { "BEAM_ID", "^[0-9]{1,}$" }
  };
  scan_config_value_patterns = {
      { "SOURCE", "^[\\w+-]+$" },
      // Regex comes from SKA Telemodel example eb-m001-20230921-12345.
      { "EB_ID", "^eb\\-[a-z0-9]+\\-[0-9]{8}\\-[a-z0-9]+$" } // NOLINT
  };
  start_scan_config_value_patterns = {
      { "SCAN_ID", "^[0-9]{1,}$" }
  };
}

ska::pst::recv::UDPReceiveDB::~UDPReceiveDB()
{
  SPDLOG_TRACE("ska::pst::recv::UDPReceiveDB::~UDPReceiveDB quit()");
  quit();
}

void ska::pst::recv::UDPReceiveDB::perform_configure_beam()
{
  ska::pst::recv::Receiver::parse_beam_config();

  if(beam_config.has("INITIAL_PACKET_TIMEOUT_THRESHOLD"))
  {
    stats.set_initial_packet_timeout_threshold(beam_config.get_uint64("INITIAL_PACKET_TIMEOUT_THRESHOLD"));
  }
  if(beam_config.has("PACKET_TIMEOUT_THRESHOLD"))
  {
    stats.set_packet_timeout_threshold(beam_config.get_uint64("PACKET_TIMEOUT_THRESHOLD"));
  }

  // extract the data and header keys from the configuration file
  std::string data_key = beam_config.get_val("DATA_KEY");
  db = std::make_unique<ska::pst::smrb::DataBlockWrite>(data_key);

  std::string weights_key = beam_config.get_val("WEIGHTS_KEY");
  wb = std::make_unique<ska::pst::smrb::DataBlockWrite>(weights_key);

  std::string udp_format = beam_config.get_val("UDP_FORMAT");
  format = ska::pst::recv::UDPFormatFactory(udp_format);

  db->connect(timeout);
  db->lock();
  db->page();

  wb->connect(timeout);
  wb->lock();
  wb->page();

  SPDLOG_DEBUG("ska::pst::recv::UDPReceiveDB::configure_beam getting data sizes");
  data.bufsz = db->get_data_bufsz();
  weights.bufsz = wb->get_data_bufsz();

  SPDLOG_DEBUG("ska::pst::recv::UDPReceiveDB::configure_beam format->configure_beam()");
  format->configure_beam(beam_config);

  // now write new params to config
  uint64_t resolution = format->get_resolution();
  beam_config.set("RESOLUTION", resolution);
  SPDLOG_DEBUG("ska::pst::recv::UDPReceiveDB::configure_beam resolution={}", resolution);
  if (data.bufsz % resolution != 0)
  {
    SPDLOG_ERROR("ska::pst::recv::UDPReceiveDB::configure_beam DB buffer size[{}] must be a multiple of RESOLUTION[{}]",
      data.bufsz, resolution);
    throw std::runtime_error("ska::pst::recv::UDPReceiveDB::configure_beam bad data buffer size");
  }

  if (weights.bufsz % format->get_weights_resolution() != 0)
  {
    SPDLOG_ERROR("ska::pst::recv::UDPReceiveDB::configure_beam weights DB buffer size[{}] must be a multiple of RESOLUTION[{}]",
      weights.bufsz, format->get_weights_resolution());
    throw std::runtime_error("ska::pst::recv::UDPReceiveDB::configure_beam bad weights DB buffer size");
  }

  uint64_t data_nresolution = data.bufsz / resolution;
  uint64_t weights_nresolution = weights.bufsz / format->get_weights_resolution();
  if (data_nresolution != weights_nresolution)
  {
    SPDLOG_ERROR("ska::pst::recv::UDPReceiveDB::configure_beam data_nresolution[{}] != weights_nresolution[{}]", data_nresolution, weights_nresolution);
    throw std::runtime_error("ska::pst::recv::UDPReceiveDB::configure_beam bad data / weights relative buffer size");
  }
  stats.configure(format.get());

  // configure the kernel buffer size for the socket
  size_t bufsz = ska::pst::recv::ideal_kernel_socket_bufsz;
  SPDLOG_DEBUG("ska::pst::recv::UDPReceiveDB::configure_beam socket->allocate_resources({}, {})", bufsz, format->get_packet_size());
  socket->allocate_resources(bufsz, format->get_packet_size());

  SPDLOG_DEBUG("ska::pst::recv::UDPReceiveDB::configure_beam configuring the socket");
  socket->configure(data_host, data_port);
}

void ska::pst::recv::UDPReceiveDB::perform_deconfigure_beam()
{
  SPDLOG_DEBUG("ska::pst::recv::UDPReceiveDB::perform_deconfigure_beam disconnecting from db");
  db->unlock();
  db->disconnect();

  SPDLOG_DEBUG("ska::pst::recv::UDPReceiveDB::perform_deconfigure_beam disconnecting from wb");
  wb->unlock();
  wb->disconnect();

  SPDLOG_DEBUG("ska::pst::recv::UDPReceiveDB::perform_deconfigure_beam socket->deconfigure_beam()");
  socket->deconfigure_beam();

  beam_config.reset();
}

void ska::pst::recv::UDPReceiveDB::perform_configure_scan()
{
  SPDLOG_DEBUG("ska::pst::recv::UDPReceiveDB::perform_configure_scan ska::pst::recv::Receiver::configure_scan()");
  ska::pst::recv::Receiver::parse_scan_config();

  SPDLOG_DEBUG("ska::pst::recv::UDPReceiveDB::perform_configure_scan set HDR_VERSION and HDR_SIZE");
  scan_config.set_val("HDR_VERSION", "1.0");
  scan_config.set("HDR_SIZE", db->get_header_bufsz());

  // determine the Telescope from the UDP Format
  std::string udp_format = scan_config.get_val("UDP_FORMAT");
  if (udp_format.find("LowPST") != std::string::npos)
  {
    scan_config.set_val("TELESCOPE", "SKALow");
  }
  else if (udp_format.find("MidPST") != std::string::npos)
  {
    scan_config.set_val("TELESCOPE", "SKAMid");
  }
  else
  {
    scan_config.set_val("TELESCOPE", "Unknown");
  }

  for (auto& key: keys_to_remove)
  {
    if (scan_config.has(key))
    {
      scan_config.del(key);
    }
  }

  SPDLOG_DEBUG("ska::pst::recv::UDPReceiveDB::perform_configure_scan format->configure_scan(scan_config)");
  format->configure_scan(scan_config);

  // write the scan configuration to the dbs
  SPDLOG_DEBUG("ska::pst::recv::UDPReceiveDB::perform_configure_scan writing config to dbs");
  db->write_config(scan_config.raw());
  wb->write_config(format->get_weights_scan_config().raw());
}

void ska::pst::recv::UDPReceiveDB::perform_deconfigure_scan()
{
  utc_start.set_time(static_cast<time_t>(0));

  SPDLOG_DEBUG("ska::pst::recv::UDPReceiveDB::perform_deconfigure format->conclude()");
  format->conclude();

  // clear the header
  SPDLOG_DEBUG("ska::pst::recv::UDPReceiveDB::perform_deconfigure resetting header and stats");
  scan_config.reset();
  stats.reset();

  // clear the data block and weights block header config
  db->clear_config();
  wb->clear_config();
}

void ska::pst::recv::UDPReceiveDB::perform_start_scan()
{
  // block accounting
  reset_accounting_meta(&data);
  reset_accounting_meta(&weights);

  SPDLOG_DEBUG("ska::pst::recv::UDPReceiveDB::perform_start_scan calling start_receiving");
  start_receiving();
  format->set_scan_id(scan_id);
  stats.reset();

  // start the monitoring thread
  monitor_thread = std::make_unique<std::thread>(std::thread(&ska::pst::recv::Receiver::monitor_method, this));
}

void ska::pst::recv::UDPReceiveDB::perform_scan()
{
  SPDLOG_DEBUG("ska::pst::recv::UDPReceiveDB::perform_scan()");

  SPDLOG_DEBUG("ska::pst::recv::UDPReceiveDB::perform_scan waiting for the start of the packet stream");
  // busy wait loop to acquire the first packet of the scan to extract the utc_start timestamp
  bool has_started = false;
  while (!has_started && socket->still_receiving())
  {
    int slot = socket->acquire_packet();

    if (slot >= 0)
    {
      char * packet_buffer = socket->get_buf_ptr(slot);
      ska::pst::recv::UDPFormat::PacketState packet_state = format->process_stream_start(packet_buffer);
      has_started = (packet_state == ska::pst::recv::UDPFormat::PacketState::Ok);
      if (has_started)
      {
        utc_start = format->get_start_timestamp();
        SPDLOG_DEBUG("ska::pst::recv::UDPReceiveDB::perform_scan has_started now true utc_start={}", utc_start.get_gmtime());
      }
      else
      {
        socket->release_packet(slot);
      }
    }
    else
    {
      #ifdef TRACE
      SPDLOG_TRACE("ska::pst::recv::UDPReceiver::perform_scan no packets available{}", slot);
      #endif
    }
  }
  SPDLOG_DEBUG("ska::pst::recv::UDPReceiveDB::perform_scan exited loop waiting for the start of the packet stream");

  SPDLOG_DEBUG("ska::pst::recv::UDPReceiveDB::perform_scan UTC_START={} ATTOSECONDS={}", utc_start.get_gmtime(), utc_start.get_fractional_time_attoseconds());

  // generate the full header to be written to the data blocks
  ska::pst::common::AsciiHeader data_header, weights_header;

  data_header.clone(scan_config);
  data_header.set_val("UTC_START", utc_start.get_gmtime());
  data_header.set("PICOSECONDS", static_cast<uint64_t>(utc_start.get_fractional_time_attoseconds() / attoseconds_per_picosecond));
  data_header.set("SCAN_ID", scan_id);

  weights_header.clone(format->get_weights_scan_config());
  weights_header.set_val("UTC_START", utc_start.get_gmtime());
  weights_header.set("PICOSECONDS", static_cast<uint64_t>(utc_start.get_fractional_time_attoseconds() / attoseconds_per_picosecond));
  weights_header.set("SCAN_ID", scan_id);

  // write the full data and weights headers to the dbs
  db->write_header(data_header.raw());
  wb->write_header(weights_header.raw());

  // open the data block for writing
  SPDLOG_DEBUG("ska::pst::recv::UDPReceiveDB::perform_scan db->open() wb->open()");
  db->open();
  wb->open();

  data.curr_buf = db->open_block();
  weights.curr_buf = wb->open_block();

  // initialise the weights scale factors in case of dropped packets
  format->clear_scales_buffer(weights.curr_buf, weights.bufsz);

  uint64_t bytes_written{0};

  SPDLOG_DEBUG("ska::pst::recv::UDPReceiveDB::perform_scan starting main loop");
  // we have control command, so start the main loop
  while (is_scanning() && socket->still_receiving())
  {
    // open the next_bufs
    data.next_buf = db->open_block();
    weights.next_buf = wb->open_block();

    // initialise the weights scale factors in case of dropped packets
    format->clear_scales_buffer(weights.next_buf, weights.bufsz);

    receive_block();

    // close the data curr_buff
    bytes_written = (socket->still_receiving()) ? data.bufsz : data.bytes_curr_buf;
    SPDLOG_DEBUG("ska::pst::recv::UDPReceiveDB::perform_scan wrote {} bytes to db", bytes_written);
    db->close_block(bytes_written);

    // close the weights curr_buff
    bytes_written = (socket->still_receiving()) ? weights.bufsz : weights.bytes_curr_buf;
    SPDLOG_DEBUG("ska::pst::recv::UDPReceiveDB::perform_scan wrote {} bytes to wb", bytes_written);
    wb->close_block(bytes_written);

    // rotate the data and weights
    rotate_accounting_meta(&data);
    rotate_accounting_meta(&weights);

    // swap curr/next buffers
    advance_accounting_buffers(&data);
    advance_accounting_buffers(&weights);
  }

  SPDLOG_DEBUG("ska::pst::recv::UDPReceiveDB::perform_scan exited main loop");

  bytes_written = data.bytes_next_buf;
  SPDLOG_DEBUG("ska::pst::recv::UDPReceiveDB::perform_scan wrote {} bytes to db", bytes_written);
  db->close_block(bytes_written);

  bytes_written = weights.bytes_next_buf;
  SPDLOG_DEBUG("ska::pst::recv::UDPReceiveDB::perform_scan wrote {} bytes to wb", bytes_written);
  wb->close_block(bytes_written);

  if (db->is_block_open())
  {
    SPDLOG_DEBUG("ska::pst::recv::UDPReceiveDB::perform_scan db->close_block({})", data.bytes_curr_buf);
    db->close_block(data.bytes_curr_buf);
    data.bytes_curr_buf = 0;
  }

  if (wb->is_block_open())
  {
    SPDLOG_DEBUG("ska::pst::recv::UDPReceiveDB::perform_scan wb->close_block({})", weights.bytes_curr_buf);
    wb->close_block(weights.bytes_curr_buf);
    weights.bytes_curr_buf = 0;
  }

  // close the dbs, causing the EoD marker to be written
  SPDLOG_DEBUG("ska::pst::recv::UDPReceiveDB::perform_scan closing data blocks");
  db->close();
  wb->close();

  SPDLOG_DEBUG("ska::pst::recv::UDPReceiveDB::perform_scan wait for StoppingScan");
}

// receive a block of UDP data
void ska::pst::recv::UDPReceiveDB::receive_block()
{
  SPDLOG_DEBUG("ska::pst::recv::UDPReceiveDB::receive_block filling data [{} - {} - {}]", data.curr_byte_offset, data.next_byte_offset, data.last_byte_offset);
  SPDLOG_DEBUG("ska::pst::recv::UDPReceiveDB::receive_block filling weights [{} - {} - {}]", weights.curr_byte_offset, weights.next_byte_offset, weights.last_byte_offset);

  const auto packet_size = static_cast<int>(format->get_packet_size());
  const auto packet_size_u64 = static_cast<uint64_t>(packet_size);
  ska::pst::recv::UDPFormat::PacketState packet_state{};
  ska::pst::recv::UDPFormat::data_and_weights_t offsets{}, received{};
  bool filled_curr_buffer = false;

  // tighter loop to fill this buffer
  while (!filled_curr_buffer && socket->still_receiving())
  {
    // get a packet from the socket
    int slot = socket->acquire_packet();

    if (slot >= 0)
    {
      // decode the header so that the format knows what to do with the packet
      packet_state = format->decode_packet(socket->get_buf_ptr(slot), &offsets, &received);

      // packet that is part of this observation
      if (packet_state == ska::pst::recv::UDPFormat::PacketState::Ok)
      {
        // packet belongs in current buffer
        if ((offsets.data >= data.curr_byte_offset) && (offsets.data < data.next_byte_offset))
        {
          data.bytes_curr_buf += received.data;
          weights.bytes_curr_buf += received.weights;

          stats.increment_bytes(packet_size_u64);
          format->insert_last_packet(data.curr_buf + (offsets.data - data.curr_byte_offset), weights.curr_buf + (offsets.weights - weights.curr_byte_offset)); // NOLINT

          socket->release_packet(slot);
        }
        // packet belongs in next buffer
        else if ((offsets.data >= data.next_byte_offset) && (offsets.data < data.last_byte_offset))
        {
          data.bytes_next_buf += received.data;
          weights.bytes_next_buf += received.weights;

          stats.increment_bytes(packet_size_u64);
          format->insert_last_packet(data.next_buf + (offsets.data - data.next_byte_offset), weights.next_buf + (offsets.weights - weights.next_byte_offset)); // NOLINT

          socket->release_packet(slot);
        }
        // ignore packets the preceed this buffer
        else if (offsets.data < data.curr_byte_offset)
        {
          #ifdef DEBUG
          SPDLOG_TRACE("offsets.data={} data.curr_byte_offset={}", offsets.data, data.curr_byte_offset);
          #endif
          socket->release_packet(slot);
          stats.misordered();
        }
        // packet is beyond next buffer
        else
        {
          #ifdef DEBUG
          SPDLOG_WARN("Received data beyond current and next buffer, dropping data");
          #endif
          filled_curr_buffer = true;
        }
      }
      else if (packet_state == ska::pst::recv::UDPFormat::PacketState::Ignored)
      {
        socket->release_packet(slot);
        stats.ignored();
      }
      else if (packet_state == ska::pst::recv::UDPFormat::PacketState::Malformed)
      {
        socket->release_packet(slot);
        stats.malformed();
      }
      else if (packet_state == ska::pst::recv::UDPFormat::PacketState::Misdirected)
      {
        socket->release_packet(slot);
        stats.misdirected();
      }

      // close open data block buffer if is is now full
      if (data.bytes_curr_buf >= static_cast<int64_t>(data.bufsz) || filled_curr_buffer || !socket->still_receiving())
      {
        SPDLOG_DEBUG("Curr {} / {} => {}", data.bytes_curr_buf, data.bufsz, ((float(data.bytes_curr_buf) / float(data.bufsz)) * percentiles_per_100));
        SPDLOG_DEBUG("Next {} / {} => {}", data.bytes_next_buf, data.bufsz, ((float(data.bytes_next_buf) / float(data.bufsz)) * percentiles_per_100));
        SPDLOG_DEBUG("ska::pst::recv::UDPReceiveDB::receive_block bytes_curr_buf={} bytes_per_buf={} bytes_next_buf={} filled_curr_buffer={}",
          data.bytes_curr_buf, data.bufsz, data.bytes_next_buf, filled_curr_buffer);
        stats.dropped_bytes(data.bufsz - data.bytes_curr_buf);
        filled_curr_buffer = true;
      }
    }
    //
    else if (slot == MALFORMED_PACKET)
    {
      SPDLOG_WARN("ska::pst::recv::UDPReceiveDB::receive_block received malformed packet");
      stats.malformed();
    }
  }
  stats.sleeps(socket->process_sleeps());
}

void ska::pst::recv::UDPReceiveDB::close_curr_bufs()
{
  SPDLOG_DEBUG("ska::pst::recv::UDPReceiveDB::close_curr_bufs data.curr_buf={}", reinterpret_cast<void *>(data.curr_buf));
  if (data.curr_buf)
  {
    SPDLOG_DEBUG("ska::pst::recv::UDPReceiveDB::close_curr_bufs close_block({})", data.bytes_curr_buf);
    db->close_block(data.bytes_curr_buf);
  }
  data.curr_buf = nullptr;

  SPDLOG_DEBUG("ska::pst::recv::UDPReceiveDB::close_curr_bufs weights.curr_buf={}", reinterpret_cast<void *>(weights.curr_buf));
  if (weights.curr_buf)
  {
    SPDLOG_DEBUG("ska::pst::recv::UDPReceiveDB::close_curr_bufs close_block({})", weights.bytes_curr_buf);
    wb->close_block(weights.bytes_curr_buf);
  }
  weights.curr_buf = nullptr;
}

auto ska::pst::recv::UDPReceiveDB::get_data_bufsz() -> uint64_t
{
  if (!is_beam_configured())
  {
    throw std::runtime_error("ska::pst::recv::UDPReceiveDB::get_data_bufsz resources were not assigned");
  }
  return db->get_data_bufsz();
}

void ska::pst::recv::UDPReceiveDB::perform_terminate()
{
  SPDLOG_DEBUG("ska::pst::recv::UDPReceiveDB::perform_terminate");
}
