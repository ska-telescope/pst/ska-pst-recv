/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <spdlog/spdlog.h>

#include "ska/pst/common/utils/ValidationContext.h"
#include "ska/pst/recv/formats/tests/MidPSTBand3Test.h"
#include "ska/pst/recv/testutils/GtestMain.h"

auto main(int argc, char* argv[]) -> int
{
  return ska::pst::recv::test::gtest_main(argc, argv);
}

namespace ska::pst::recv::test {

MidPSTBand3Test::MidPSTBand3Test()
    : ::testing::Test()
{
  config.load_from_file(test_data_file("MidPSTBand3Config.txt"));
}

void MidPSTBand3Test::SetUp()
{
}

void MidPSTBand3Test::TearDown()
{
}

TEST_F(MidPSTBand3Test, test_default_constructor) // NOLINT
{
  MidPSTBand3 format;
}

TEST_F(MidPSTBand3Test, test_configure) // NOLINT
{
  MidPSTBand3 format;
  format.configure_beam(config);
}

TEST_F(MidPSTBand3Test, test_bad_configuration_os_factor) // NOLINT
{
  MidPSTBand3 format;

  // expect default format is OK
  EXPECT_NO_THROW(format.configure_beam(config)); // NOLINT

  // test OS denominator not 8/7
  format.reset();
  config.set_val("OS_FACTOR", "8/100");
  EXPECT_THROW(format.configure_beam(config), ska::pst::common::pst_validation_error); // NOLINT

  // test OS numerator
  format.reset();
  config.set_val("OS_FACTOR", "100/7");
  EXPECT_THROW(format.configure_beam(config), ska::pst::common::pst_validation_error); // NOLINT
}


TEST_F(MidPSTBand3Test, test_bad_configuration) // NOLINT
{
  MidPSTBand3 format;
  config.set_val("NBIT", "15");
  EXPECT_THROW(format.configure_beam(config), ska::pst::common::pst_validation_error); // NOLINT
}

} // namespace ska::pst::recv::test