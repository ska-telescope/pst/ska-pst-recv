/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <spdlog/spdlog.h>

#include "ska/pst/common/utils/ValidationContext.h"
#include "ska/pst/recv/formats/tests/LowTestVectorTest.h"
#include "ska/pst/recv/formats/UDPHeader.h"
#include "ska/pst/recv/testutils/GtestMain.h"

auto main(int argc, char* argv[]) -> int
{
  return ska::pst::recv::test::gtest_main(argc, argv);
}

namespace ska::pst::recv::test {

LowTestVectorTest::LowTestVectorTest()
    : ::testing::Test()
{
  config.load_from_file(test_data_file("LowTestVectorConfig.txt"));
  header.load_from_file(test_data_file("Header.txt"));
}

void LowTestVectorTest::SetUp()
{
}

void LowTestVectorTest::TearDown()
{
}

TEST_F(LowTestVectorTest, test_default_constructor) // NOLINT
{
  LowTestVector format;
}

TEST_F(LowTestVectorTest, test_configure) // NOLINT
{
  LowTestVector format;
  format.configure_beam(config);
}

TEST_F(LowTestVectorTest, test_bad_configuration) // NOLINT
{
  LowTestVector format;
  config.set_val("NBIT", "15");
  EXPECT_THROW(format.configure_beam(config), ska::pst::common::pst_validation_error); // NOLINT
}

TEST_F(LowTestVectorTest, test_get_samples_per_packet) // NOLINT
{
  LowTestVector format;
  format.configure_beam(config);
  static constexpr unsigned samples_per_packet = 32;
  EXPECT_EQ(format.get_samples_per_packet(), samples_per_packet);
}

TEST_F(LowTestVectorTest, test_gen_packet) // NOLINT
{
  LowTestVector format;
  format.configure_beam(config);
  format.configure_scan(header);

  static constexpr uint64_t scan_id = 2345;
  format.set_scan_id(scan_id);

  UDPHeader udp_header;
  udp_header.configure(config, format);
  udp_header.set_scan_id(scan_id);
  udp_header.set_beam_id(config.get_uint32("BEAM_ID"));

  // allocate storage for the packet
  std::vector<char> packet_data(format.get_packet_size());
  auto psr_header = reinterpret_cast<cbf_psr_header_t *>(&packet_data[0]);
  auto char_header = reinterpret_cast<char *>(&packet_data[0]);
  ska::pst::recv::UDPFormat::data_and_weights_t offsets{}, sizes{};

  udp_header.encode_header(char_header);

  EXPECT_EQ(format.process_stream_start(char_header), ska::pst::recv::UDPFormat::PacketState::Ok);

  // record the starting timestamp
  uint32_t timestamp_seconds = psr_header->timestamp_seconds;
  uint32_t nchan = config.get_uint32("NCHAN");
  uint32_t nchan_per_packet = format.get_nchan_per_packet();
  uint32_t npackets = nchan / nchan_per_packet;

  uint32_t frame = 0;
  uint32_t psn = 0;
  uint32_t payload_size = 0;
  while (psr_header->timestamp_seconds == timestamp_seconds)
  {
    // generate 1 full set for channels in the packet sequence
    for (unsigned i=0; i<npackets; i++)
    {
      udp_header.gen_packet(char_header);
      ASSERT_EQ(format.decode_packet(char_header, &offsets, &sizes), ska::pst::recv::UDPFormat::PacketState::Ok);
      ASSERT_EQ(psn * format.get_packet_data_size(), offsets.data);
      ASSERT_EQ(psn * (format.get_packet_weights_size() + format.get_packet_scales_size()), offsets.weights);
      ASSERT_EQ(sizes.data, format.get_packet_data_size());
      ASSERT_EQ(sizes.weights, format.get_packet_weights_size() + format.get_packet_scales_size());
      psn++;
      ASSERT_EQ(psr_header->packet_sequence_number, psn);
    }
    frame++;
  }
}

TEST_F(LowTestVectorTest, test_decode_packet) // NOLINT
{
  LowTestVector format;
  format.configure_beam(config);
  format.configure_scan(header);

  static constexpr uint64_t scan_id = 2345;
  format.set_scan_id(scan_id);

  UDPHeader udp_header;
  udp_header.configure(config, format);
  udp_header.set_scan_id(scan_id);
  udp_header.set_beam_id(config.get_uint32("BEAM_ID"));

  ska::pst::recv::UDPFormat::data_and_weights_t offsets{}, sizes{};
  std::vector<char> packet_data(format.get_packet_size());
  auto psr_header = reinterpret_cast<cbf_psr_header_t *>(&packet_data[0]);
  auto char_header = reinterpret_cast<char *>(&packet_data[0]);

  udp_header.encode_header(char_header);

  // calls to decode packet before the timestamp is received should return an error
  EXPECT_THROW(format.decode_packet(char_header, &offsets, &sizes), std::runtime_error); // NOLINT

  // test false is returned whilst the channel number is not 0
  psr_header->first_channel_number = 1;
  psr_header->packet_sequence_number = 1;
  EXPECT_EQ(format.process_stream_start(char_header), ska::pst::recv::UDPFormat::PacketState::Ignored);

  // set the first channel number of the packet to 0
  psr_header->first_channel_number = 0;
  EXPECT_EQ(format.process_stream_start(char_header), ska::pst::recv::UDPFormat::PacketState::Ok);

  ska::pst::common::Time utc_start = format.get_start_timestamp();
  SPDLOG_DEBUG("ska::pst::recv::test::LowTestVectorTest::test_decode_packet utc_start={}", utc_start.get_gmtime());

  EXPECT_EQ(format.decode_packet(char_header, &offsets, &sizes), ska::pst::recv::UDPFormat::PacketState::Ok);
  EXPECT_EQ(offsets.data, 0);
  EXPECT_EQ(offsets.weights, 0);

  std::vector<char> output_data(format.get_resolution());
  std::vector<char> output_weights(format.get_weights_resolution());
  EXPECT_NO_THROW(format.insert_last_packet(&output_data[0], &output_weights[0])); // NOLINT
}

} // namespace ska::pst::recv::test
