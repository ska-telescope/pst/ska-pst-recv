/*
 * Copyright 2023 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdexcept>
#include <spdlog/spdlog.h>

#include "ska/pst/common/utils/AsciiHeader.h"
#include "ska/pst/recv/definitions.h"
#include "ska/pst/recv/formats/UDPHeader.h"

ska::pst::recv::UDPHeader::UDPHeader() :
  header(INIT_CBF_PST_HEADER_T)
{
  configure_packet_defaults(&header);
}

void ska::pst::recv::UDPHeader::configure(const ska::pst::common::AsciiHeader& config, const ska::pst::recv::UDPFormat& format)
{
  double bw = config.get_double("BW");
  double freq = config.get_double("FREQ");
  double tsamp = config.get_double("TSAMP");
  uint32_t nchan = config.get_uint32("NCHAN");
  uint32_t nbit = config.get_uint32("NBIT");

  start_channel = config.get_uint32("START_CHANNEL");
  end_channel = config.get_uint32("END_CHANNEL");

  // the number of attoseconds that increment with each packet
  attoseconds_per_packet = static_cast<double>(rint(ska::pst::recv::attoseconds_per_microsecond * format.get_samples_per_packet() * tsamp));
  nchan_per_packet = format.get_nchan_per_packet();
  packet_size = format.get_packet_size();
  increment_psn_every_packet = format.get_increment_psn_every_packet();

  // configure the defaults for the UDP packets
  header.first_channel_number = start_channel;
  header.scale_1 = 1.0f;
  header.scale_2 = 1.0f;
  header.scale_3 = 1.0f;
  header.scale_4 = 1.0f;
  header.offset_1 = 0.0f;
  header.offset_2 = 0.0f;
  header.offset_3 = 0.0f;
  header.offset_4 = 0.0f;
  header.time_samples_per_packet = static_cast<uint16_t>(format.get_samples_per_packet());
  header.channels_per_packet = static_cast<uint16_t>(format.get_nchan_per_packet());
  header.valid_channels_per_packet = static_cast<uint16_t>(nchan_per_packet);
  header.num_power_samples_averaged = static_cast<uint8_t>(0);
  header.num_time_samples_per_relative_weight = static_cast<uint8_t>(format.get_nsamp_per_weight());
  header.channel_separation = static_cast<uint32_t>(rint((freq / nchan) * ska::pst::recv::millihertz_per_megahertz));
  header.first_channel_freq = static_cast<uint64_t>(rint((freq - (bw / 2)) * ska::pst::recv::millihertz_per_megahertz));
  header.data_precision = static_cast<uint8_t>(nbit);
  header.os_ratio_numerator = static_cast<uint8_t>(format.get_os_numerator());
  header.os_ratio_denominator = static_cast<uint8_t>(format.get_os_denominator());
  header.packet_destination = static_cast<uint8_t>(format.get_destination());
  header.beam_number = config.get_uint32("BEAM_ID");
  configured = true;
}

void ska::pst::recv::UDPHeader::set_timestamp(ska::pst::common::Time& start_timestamp)
{
  header.timestamp_seconds = static_cast<uint32_t>(start_timestamp.get_time());
  header.timestamp_attoseconds = static_cast<uint64_t>(start_timestamp.get_fractional_time_attoseconds());
  SPDLOG_DEBUG("ska::pst::recv::UDPHeader::set_packet_timestamp timestamp_seconds={} timestamp_attoseconds={}", static_cast<uint64_t>(header.timestamp_seconds), static_cast<uint64_t>(header.timestamp_attoseconds));
}

void ska::pst::recv::UDPHeader::set_packet_sequence_number(uint64_t psn)
{
  SPDLOG_DEBUG("ska::pst::recv::UDPHeader::set_packet_sequence_number({})", psn);
  header.packet_sequence_number = psn;
}

void ska::pst::recv::UDPHeader::set_scan_id(uint64_t scan_id)
{
  SPDLOG_DEBUG("ska::pst::recv::UDPHeader::set_scan_id({})", scan_id);
  header.scan_id = scan_id;
}

void ska::pst::recv::UDPHeader::set_beam_id(uint64_t beam_id)
{
  header.beam_number = beam_id;
}

void ska::pst::recv::UDPHeader::encode_header(char * buf)
{
  if (!configured)
  {
    SPDLOG_ERROR("ska::pst::recv::UDPHeader::encode_header UDPHeader not configured");
    throw std::runtime_error("Cannot call encode_header if UDP Header not configured");
  }
  memcpy(static_cast<void *>(buf), static_cast<void *>(&header), sizeof(ska::pst::recv::cbf_psr_header_t)); // NOLINT
}

auto ska::pst::recv::UDPHeader::gen_packet(char * buf) -> size_t
{
  // write local header to the socket buffer
  encode_header(buf);

  // increment the packet counters
  increment_packet();

  return packet_size;
}

void ska::pst::recv::UDPHeader::increment_packet()
{
  // increment the first channel number
  if (increment_psn_every_packet)
  {
    header.packet_sequence_number++;
  }

  header.first_channel_number += nchan_per_packet;

  // if all channels have been encoded and sent
  if (header.first_channel_number >= end_channel)
  {
    header.first_channel_number = start_channel;

    if (!increment_psn_every_packet)
    {
      header.packet_sequence_number++;
    }

    // update the packet timestamps
    header.timestamp_attoseconds += static_cast<uint64_t>(attoseconds_per_packet);
    while (header.timestamp_attoseconds >= static_cast<uint64_t>(attoseconds_per_second))
    {
      header.timestamp_seconds += 1;
      header.timestamp_attoseconds -= static_cast<uint64_t>(attoseconds_per_second);
    }
  }

  // ensure the packet final channel doesn't extend past the end_chan
  if (header.first_channel_number + header.channels_per_packet >= end_channel)
  {
    header.channels_per_packet = end_channel - header.first_channel_number;
  }
}

auto ska::pst::recv::UDPHeader::gen_packet_failure(char * buf, ska::pst::recv::FailureType failure_type) -> size_t
{
  cbf_psr_header_t tmp_header{};

  if (failure_type == BadTransmit)
  {
    SPDLOG_DEBUG("ska::pst::recv::UDPHeader::gen_packet_failure increment_packet due to BadTransmit");
    increment_packet();
  }

  memcpy(static_cast<void *>(&tmp_header), static_cast<void *>(&header), sizeof(ska::pst::recv::cbf_psr_header_t)); // NOLINT

  size_t pkt_size = packet_size;

  switch (failure_type)
  {
    case BadMagicWord:
      // modify the magic_word in the header
      SPDLOG_DEBUG("ska::pst::recv::UDPHeader::gen_packet_failure process BadMagicWord");
      header.magic_word = ska::pst::recv::magic_word + 1;
      break;

    case BadPacketSize:
      // return an incorrect packet size
      SPDLOG_DEBUG("ska::pst::recv::UDPHeader::gen_packet_failure process BadPacketSize");
      pkt_size -= 1;
      break;

    case BadTransmit:
      // skip the transmission of a packet in the sequence, see above
      SPDLOG_DEBUG("ska::pst::recv::UDPHeader::gen_packet_failure process BadTransmit");
      break;

    case MisorderedPacketSequenceNumber:
      SPDLOG_DEBUG("ska::pst::recv::UDPHeader::gen_packet_failure process MisorderedPacketSequenceNumber");
      static constexpr uint64_t misordered_psn_offset = 1;
      header.packet_sequence_number = misordered_psn_offset;
      break;

    case BadScanID:
      SPDLOG_DEBUG("ska::pst::recv::UDPHeader::gen_packet_failure process BadScanID");
      header.scan_id += 1;
      break;

    case BadChannelNumber:
      SPDLOG_DEBUG("ska::pst::recv::UDPHeader::gen_packet_failure process BadChannelNumber");
      static constexpr uint32_t bad_channel_offset = 1000000;
      header.first_channel_number += bad_channel_offset;
      break;

    case BadTimestamp:
      SPDLOG_DEBUG("ska::pst::recv::UDPHeader::gen_packet_failure process BadTimestamp");
      static constexpr uint32_t bad_timestamp_offset = 1000000;
      header.timestamp_seconds += bad_timestamp_offset;
      break;

    case BadDataRate:
      SPDLOG_DEBUG("ska::pst::recv::UDPHeader::gen_packet_failure process BadBadDataRateTimestamp");
      break;

    default:
      break;
  }

  // encode the bad header
  encode_header(buf);

  // restore the original header
  memcpy(static_cast<void *>(&header), static_cast<void *>(&tmp_header), sizeof(ska::pst::recv::cbf_psr_header_t)); // NOLINT

  // increment the packet counters
  increment_packet();

  return pkt_size;
}