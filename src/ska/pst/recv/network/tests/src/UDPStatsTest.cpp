/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <spdlog/spdlog.h>

#include "ska/pst/recv/testutils/GtestMain.h"
#include "ska/pst/recv/network/tests/UDPStatsTest.h"

auto main(int argc, char* argv[]) -> int
{
  return ska::pst::recv::test::gtest_main(argc, argv);
}

namespace ska::pst::recv::test {

UDPStatsTest::UDPStatsTest()
    : ::testing::Test()
{
}

void UDPStatsTest::SetUp()
{
  config.load_from_file(test_data_file("LowTestVectorConfig.txt"));
  format.configure_beam(config);
}

void UDPStatsTest::TearDown()
{
}

TEST_F(UDPStatsTest, test_default_constructor) // NOLINT
{
  UDPStats stats;
  ASSERT_EQ(stats.get_nsleeps(), 0);
  ASSERT_EQ(stats.get_data_transmitted(), 0);
  ASSERT_EQ(stats.get_data_dropped(), 0);
}

TEST_F(UDPStatsTest, test_unconfigured) // NOLINT
{
  UDPStats stats;
  EXPECT_THROW(stats.get_packets_transmitted(), std::runtime_error); // NOLINT
  EXPECT_THROW(stats.get_packets_dropped(), std::runtime_error); // NOLINT
}

TEST_F(UDPStatsTest, test_configure) // NOLINT
{
  UDPStats stats;
  stats.configure(&format);
}

TEST_F(UDPStatsTest, test_reset) // NOLINT
{
  UDPStats stats;
  stats.configure(&format);

  stats.increment();
  stats.dropped();
  stats.sleeps(1);

  ASSERT_EQ(stats.get_nsleeps(), 1);
  ASSERT_EQ(stats.get_packets_transmitted(), 1);
  ASSERT_EQ(stats.get_data_transmitted(), format.get_packet_size());
  ASSERT_EQ(stats.get_packets_dropped(), 1);
  ASSERT_EQ(stats.get_data_dropped(), format.get_packet_size());

  stats.reset();

  ASSERT_EQ(stats.get_nsleeps(), 0);
  ASSERT_EQ(stats.get_packets_transmitted(), 0);
  ASSERT_EQ(stats.get_packets_dropped(), 0);
  ASSERT_EQ(stats.get_data_transmitted(), 0);
  ASSERT_EQ(stats.get_data_dropped(), 0);
}

TEST_F(UDPStatsTest, test_compute_rates) // NOLINT
{
  UDPStats stats;
  stats.configure(&format);
  stats.reset();
  stats.compute_rates();

  ASSERT_EQ(stats.get_nsleeps(), 0);
  ASSERT_EQ(stats.get_packets_transmitted(), 0);
  ASSERT_EQ(stats.get_packets_dropped(), 0);
  ASSERT_EQ(stats.get_data_transmitted(), 0);
  ASSERT_EQ(stats.get_data_dropped(), 0);
}

TEST_F(UDPStatsTest, test_get_invalid_packets ) // NOLINT
{
  UDPStats stats;
  stats.configure(&format);
  stats.reset();
  stats.compute_rates();

  stats.malformed();
  ASSERT_EQ(1, stats.get_malformed());
  stats.misordered();
  ASSERT_EQ(1, stats.get_misordered());
  stats.misdirected();
  ASSERT_EQ(1, stats.get_misdirected());
  stats.discarded();
  ASSERT_EQ(1, stats.get_discarded());

  stats.reset();
  ASSERT_EQ(0, stats.get_malformed());
  ASSERT_EQ(0, stats.get_misordered());
  ASSERT_EQ(0, stats.get_misdirected());
  ASSERT_EQ(0, stats.get_discarded());
}

} // namespace ska::pst::recv::test