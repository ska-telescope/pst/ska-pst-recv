/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <spdlog/spdlog.h>

#include "ska/pst/recv/testutils/GtestMain.h"
#include "ska/pst/recv/network/tests/EndianTest.h"

auto main(int argc, char* argv[]) -> int
{
  return ska::pst::recv::test::gtest_main(argc, argv);
}

static constexpr unsigned u32_offset = 4;
static constexpr unsigned u16_offset = 6;

namespace ska::pst::recv::test {

EndianTest::EndianTest()
    : ::testing::Test()
{
}

void EndianTest::SetUp()
{
  size_t max_length = sizeof(uint64_t);
  raw_le.resize(max_length);
  raw_be.resize(max_length);
  for (unsigned i=0; i<max_length; i++)
  {
    raw_le[i] = static_cast<char>(i);
    raw_be[i] = static_cast<char>((max_length-1)-i);
  }
}

void EndianTest::TearDown()
{
}

TEST_F(EndianTest, test_betoh) // NOLINT
{
  auto le_u64_ptr = reinterpret_cast<uint64_t *>(&raw_le[0]);
  auto le_u32_ptr = reinterpret_cast<uint32_t *>(&raw_le[u32_offset]);
  auto le_u16_ptr = reinterpret_cast<uint16_t *>(&raw_le[u16_offset]);

  uint64_t le_u64_val = *le_u64_ptr;
  uint32_t le_u32_val = *le_u32_ptr;
  uint16_t le_u16_val = *le_u16_ptr;

  uint64_t be_u64_val = htobe(le_u64_val);
  uint64_t be_u32_val = htobe(le_u32_val);
  uint64_t be_u16_val = htobe(le_u16_val);

  auto be_u64_ptr = reinterpret_cast<uint64_t *>(&raw_be[0]);
  auto be_u32_ptr = reinterpret_cast<uint32_t *>(&raw_be[0]);
  auto be_u16_ptr = reinterpret_cast<uint16_t *>(&raw_be[0]);

  ASSERT_EQ(be_u64_val, *be_u64_ptr);
  ASSERT_EQ(be_u32_val, *be_u32_ptr);
  ASSERT_EQ(be_u16_val, *be_u16_ptr);
}

TEST_F(EndianTest, test_htobe) // NOLINT
{
  auto be_u64_ptr = reinterpret_cast<uint64_t *>(&raw_be[0]);
  auto be_u32_ptr = reinterpret_cast<uint32_t *>(&raw_be[u32_offset]);
  auto be_u16_ptr = reinterpret_cast<uint16_t *>(&raw_be[u16_offset]);

  uint64_t be_u64_val = *be_u64_ptr;
  uint32_t be_u32_val = *be_u32_ptr;
  uint16_t be_u16_val = *be_u16_ptr;

  uint64_t le_u64_val = betoh(be_u64_val);
  uint64_t le_u32_val = betoh(be_u32_val);
  uint64_t le_u16_val = betoh(be_u16_val);

  auto le_u64_ptr = reinterpret_cast<uint64_t *>(&raw_le[0]);
  auto le_u32_ptr = reinterpret_cast<uint32_t *>(&raw_le[0]);
  auto le_u16_ptr = reinterpret_cast<uint16_t *>(&raw_le[0]);

  ASSERT_EQ(le_u64_val, *le_u64_ptr);
  ASSERT_EQ(le_u32_val, *le_u32_ptr);
  ASSERT_EQ(le_u16_val, *le_u16_ptr);
}


} // namespace ska::pst::recv::test