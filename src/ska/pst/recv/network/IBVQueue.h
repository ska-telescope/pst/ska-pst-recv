/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <memory>
#include <string>

#include "ska/pst/recv/network/SocketReceive.h"
#include "ska/pst/recv/network/IBVUtils.h"
#include "ska/pst/recv/network/PacketUtils.h"

#ifndef SKA_PST_RECV_NETWORK_IBVSocket_h
#define SKA_PST_RECV_NETWORK_IBVSocket_h

namespace ska::pst::recv {

  /**
   * @brief Implementation for a UDP socket that uses Infiniband Verbs to bypass
   * the kernel socket library
   */
  class IBVQueue : public SocketReceive {

    public:

      /**
       * @brief Maximum number of packet slots in the IBVQueue.
       * This maximum is chosen at a lower value to work with a larger number of 
       * Mellanox NICs.
       * 
       */
      static constexpr size_t max_queue_length = 8192;

      /**
       * @brief Construct a new IBVQueue object
       *
       */
      IBVQueue() = default;

      /**
       * @brief Destroy the IBVQueue object
       *
       */
      ~IBVQueue();

      /**
       * @brief Configure the memory queue
       *
       * @param npackets number of UDP packets in the memory queue
       * @param packet_size maximum size of the UDP payload
       */
      void allocate_resources(size_t npackets, size_t packet_size);

      /**
       * @brief Allocate the receive queue memory slots
       *
       */
      void configure(const std::string& ip_address, int port);

      /**
       * @brief Release all resources associated with the IBVQueue
       * 
       */
      void deconfigure_beam();

      /**
       * @brief Discard all packets that are waiting in the kernel's socket buffer.
       * 
       * @return size_t number of bytes cleared.
       */
      size_t clear_buffered_packets();

      /**
       * @brief Open the IBV Queue at the specified endpoint
       *
       * @param ipv4_address IPv4 address of the local endpoint at which to receive packets
       * @param port UDP port of the local endpoint at which to receive packets
       */
      void open(const std::string& ipv4_address, int port);

      /**
       * @brief Receive a single UDP packet.
       * If the previously received packet was not consumed via consume_packet, then no packet is received.
       * If the socket is blocking, this method will block until a packet is received.
       * If the socket is non-blocking, this method will busy-wait until a packet is received.
       * In the latter case, the busy-wait may be interrupted by setting SocketReceiver::keep_receiving to false. 
       *
       * @return int packet slot index of the acquired packet, less than 0 indicates error
       */
      int acquire_packet();

      /**
       * @brief Release the most recently received packet
       * 
       * @param index slot
       */
      void release_packet(int index);

      /**
       * @brief Returns the number of busy-waits that queue has recorded.
       * Resets the busy-wait counter to zero.
       * 
       * @return uint64_t number of busy-wait sleeps.
       */
      uint64_t process_sleeps();

      /**
       * @brief Return a pointer to the packet that is received in the specified slot
       * 
       * @param slot_index 
       * @return char *
       */
      char * get_buf_ptr(int slot_index);

    protected:

    private:

      //! memory slots
      std::vector<char> buffer;

      //! Maximum valid size of a packet in the queue
      size_t max_raw_size{0};

      //! Minimum valid size of a packet in the queue
      size_t min_raw_size{0};

      //! Number of slots in the queue
      size_t n_slots{0};

      //! RDMA event channel
      std::unique_ptr<RDMAEventChannel> ec{nullptr};

      //! RDMA Communication Manager
      std::unique_ptr<RDMACommunicationManager> cm{nullptr};

      //! IBV Completion Queue
      std::unique_ptr<IBVCompletionQueue> cq{nullptr};

      //! IBV Protection Domain
      std::unique_ptr<IBVProtectionDomain> pd{nullptr};

      //! IBV Queue Pair
      std::unique_ptr<IBVQueuePair> qp{nullptr};

      //! IBV Memory Region
      std::unique_ptr<IBVMemoryRegion> mr{nullptr};

      //! IBV Flow
      std::unique_ptr<IBVFlow> fl{nullptr};

      //! Packet Buffer
      PacketBuffer payload;

      //! array of slots for work requests
      std::vector<ibv_slot_t> slots;

      //! array of work completions
      std::vector<ibv_wc> wc;

      //! array of work completions
      std::vector<char *> ptrs;

      //! slot of currently opened packet, -1 if not open
      int curr_slot{-1};

      //! index of next available packet to process
      int ipacket{0};

      //! number of packets available the queue
      int npackets{0};

  };

} // namespace ska::pst::recv

#endif // SKA_PST_RECV_NETWORK_IBVQueue_h
