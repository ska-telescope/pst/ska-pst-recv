/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <cstdlib>
#include <cstring>
#include <cerrno>
#include <stdexcept>
#include <spdlog/spdlog.h>

#include "ska/pst/recv/network/UDPSocketSend.h"

ska::pst::recv::UDPSocketSend::UDPSocketSend() :
  sock_addr(reinterpret_cast<struct sockaddr *>(&udp_sock)),
  sock_size(sizeof(struct sockaddr))
{
}

void ska::pst::recv::UDPSocketSend::open(const std::string &dest_ipv4_address, int dest_port, const std::string &local_ipv4_address)
{
  SPDLOG_DEBUG("ska::pst::recv::UDPSocketSend::open sending to {}:{} from {}", dest_ipv4_address, dest_port, local_ipv4_address);

  // open the socket FD
  ska::pst::recv::UDPSocket::open(dest_port);

  // transmitting sockets must have an IP specified in the destination udp_sock
  int valid = inet_aton(dest_ipv4_address.c_str(), &udp_sock.sin_addr);
  if (valid == 0)
  {
    SPDLOG_ERROR("ska::pst::recv::UDPSocketSend::open unable to interpret {} as valid IP address", dest_ipv4_address);
    throw std::runtime_error("Unable to interpet destination IPv4 address");
  }

  if (local_ipv4_address.compare("any") == 0)
  {
    other_udp_sock.sin_addr.s_addr = htonl(INADDR_ANY);
  }
  else
  {
    other_udp_sock.sin_addr.s_addr = inet_addr(local_ipv4_address.c_str());
    if (other_udp_sock.sin_addr.s_addr == -1)
    {
      SPDLOG_ERROR("ska::pst::recv::UDPSocketSend::open unable to interpret {} as valid IP address", local_ipv4_address);
      throw std::runtime_error("Unable to interpet local IPv4 address");
    }
  }

  // bind socket to file descriptor
  if (bind(fd, reinterpret_cast<struct sockaddr *>(&other_udp_sock), sizeof(other_udp_sock)) == -1)
  {
    SPDLOG_ERROR("ska::pst::recv::UDPSocketSend::open unable to bind to UDP socket");
    throw std::runtime_error("could not bind to UDP socket");
  }
}

auto ska::pst::recv::UDPSocketSend::send() -> size_t
{
  ssize_t rval = sendto(fd, &buf[0], buf.size(), 0, sock_addr, sock_size);
  if (rval == -1)
  {
    SPDLOG_WARN("ska::pst::recv::UDPSocketSend::send sendto failed: {}", strerror(errno));
    throw std::runtime_error("failed to send UDP packet");
  }
  return rval;
}

auto ska::pst::recv::UDPSocketSend::send(size_t nbytes) -> size_t
{
  if (nbytes > buf.size())
  {
    throw std::runtime_error("cannot send more bytes than socket size");
  }
  ssize_t rval = sendto(fd, &buf[0], nbytes, 0, sock_addr, sock_size);
  if (rval == -1)
  {
    SPDLOG_WARN("ska::pst::recv::UDPSocketSend::send sendto_failed: {}", strerror(errno));
    throw std::runtime_error("failed to send UDP packet");
  }
  return static_cast<size_t>(rval);
}
