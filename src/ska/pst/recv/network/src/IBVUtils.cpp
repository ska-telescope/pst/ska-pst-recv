/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <rdma/rdma_cma.h>
#include <netdb.h>
#include <netinet/in.h>
#include <infiniband/verbs.h>

#include <stdexcept>
#include <cstdlib>
#include <cerrno>
#include <iostream>
#include <cstring> // memset
#include <spdlog/spdlog.h>

#include "ska/pst/recv/network/IBVUtils.h"
#include "ska/pst/recv/network/PacketUtils.h"

ska::pst::recv::RDMAEventChannel::RDMAEventChannel()
{
  SPDLOG_TRACE("ska::pst::recv::RDMAEventChannel::RDMAEventChannel rdma_create_event_channel()");
  errno = 0;
  ec = rdma_create_event_channel(); // NOLINT
  if (!ec)
  {
    SPDLOG_ERROR("ska::pst::recv::RDMAEventChannel::RDMAEventChannel rdma_create_event_channel failed errno={}", errno);
    throw std::runtime_error("ska::pst::recv::RDMAEventChannel::RDMAEventChannel failed to create RDMA Event Channel");
  }
  SPDLOG_DEBUG("ska::pst::recv::RDMAEventChannel::RDMAEventChannel ec={}", reinterpret_cast<void *>(ec));
}

ska::pst::recv::RDMAEventChannel::~RDMAEventChannel()
{
  SPDLOG_TRACE("ska::pst::recv::RDMAEventChannel::~RDMAEventChannel");
  if (ec)
  {
    rdma_destroy_event_channel(ec);
  }
  ec = nullptr;
}

ska::pst::recv::RDMACommunicationManager::RDMACommunicationManager(ska::pst::recv::RDMAEventChannel * ec)
{
  SPDLOG_TRACE("ska::pst::recv::RDMACommunicationManager::RDMACommunicationManager ctor");
  int status = rdma_create_id(ec->get(), &cm_id, nullptr, RDMA_PS_UDP);
  if (status != 0)
  {
    throw std::runtime_error("ska::pst::recv::RDMACommunicationManager::RDMACommunicationManager rdma_create_id failed");
  }
  SPDLOG_DEBUG("ska::pst::recv::RDMACommunicationManager::RDMACommunicationManager cm_id={} cm_id->verbs={}",
    reinterpret_cast<void *>(cm_id), reinterpret_cast<void *>(cm_id->verbs));
}

ska::pst::recv::RDMACommunicationManager::~RDMACommunicationManager()
{
  SPDLOG_TRACE("ska::pst::recv::RDMACommunicationManager::~RDMACommunicationManager");
  if (cm_id)
  {
    rdma_destroy_id(cm_id);
  }
  cm_id = nullptr;
}

void ska::pst::recv::RDMACommunicationManager::bind(const std::string &ip_addr)
{
  sockaddr_in sock{0};
  memset(&(sock.sin_zero), 0, sizeof(sock.sin_zero));
  sock.sin_family = AF_INET;
  sock.sin_port = htons(0);
  sock.sin_addr.s_addr = inet_addr(ip_addr.c_str());

  SPDLOG_TRACE("ska::pst::recv::RDMACommunicationManager::bind rdma_bind_addr()");
  int status = rdma_bind_addr(cm_id, (struct sockaddr *) &sock); // NOLINT
  if (status != 0)
  {
    throw std::runtime_error("ska::pst::recv::RDMACommunicationManager::bind rdma_bind_addr failed");
  }
  if (cm_id->verbs == nullptr)
  {
    throw std::runtime_error("ska::pst::recv::RDMACommunicationManager::bind could not bind to " + ip_addr);
  }
  SPDLOG_DEBUG("ska::pst::recv::RDMACommunicationManager::bind status={} cm_id={} cm_id->verbs={}", status,
    reinterpret_cast<void *>(cm_id), reinterpret_cast<void *>(cm_id->verbs));
}

ska::pst::recv::IBVCompletionQueue::IBVCompletionQueue(ska::pst::recv::RDMACommunicationManager * cm, int n_slots)
{
  SPDLOG_TRACE("ska::pst::recv::IBVCompletionQueue::IBVCompletionQueue nslots={} cm_id={} cm_id->verbs={}",
    n_slots, reinterpret_cast<void *>(cm->get()), reinterpret_cast<void *>(cm->get()->verbs));
  errno = 0;
  cq = ibv_create_cq(cm->get()->verbs, n_slots, nullptr, nullptr, 0); // NOLINT
  if (!cq)
  {
    SPDLOG_ERROR("ska::pst::recv::IBVCompletionQueue::IBVCompletionQueue ibv_create_cq failed errno={}", errno);
    throw std::runtime_error("ska::pst::recv::IBVCompletionQueue::IBVCompletionQueue ibv_create_cq");
  }
  SPDLOG_DEBUG("ska::pst::recv::IBVCompletionQueue::IBVCompletionQueue cq={}", reinterpret_cast<void *>(cq));
}

ska::pst::recv::IBVCompletionQueue::~IBVCompletionQueue()
{
  SPDLOG_TRACE("ska::pst::recv::IBVCompletionQueue::~IBVCompletionQueue");
  if (cq)
  {
    ibv_destroy_cq(cq);
  }
  cq = nullptr;
}

auto ska::pst::recv::IBVCompletionQueue::poll(int num_entries, ibv_wc *wc) -> int
{
  int received = ibv_poll_cq(cq, num_entries, wc);
  if (received < 0)
  {
    throw std::runtime_error("ska::pst::recv::IBVCompletionQueue::poll ibv_poll_cq failed");
  }
  return received;
}

ska::pst::recv::IBVProtectionDomain::IBVProtectionDomain(ska::pst::recv::RDMACommunicationManager * cm)
{
  SPDLOG_TRACE("ska::pst::recv::IBVProtectionDomain::IBVProtectionDomain ibv_alloc_pd()");
  errno = 0;
  pd = ibv_alloc_pd(cm->get()->verbs); // NOLINT
  if (!pd)
  {
    SPDLOG_ERROR("ska::pst::recv::IBVProtectionDomain::IBVProtectionDomain ibv_alloc_pd failed errno={}", errno);
    throw std::runtime_error("ska::pst::recv::IBVProtectionDomain::IBVProtectionDomain ibv_alloc_pd failed");
  }
  SPDLOG_DEBUG("ska::pst::recv::IBVProtectionDomain::IBVProtectionDomain pd={}", reinterpret_cast<void *>(pd));
}

ska::pst::recv::IBVProtectionDomain::~IBVProtectionDomain()
{
  SPDLOG_TRACE("ska::pst::recv::IBVProtectionDomain::~IBVProtectionDomain");
  if (pd)
  {
    ibv_dealloc_pd(pd);
  }
  pd = nullptr;
}

ska::pst::recv::IBVQueuePair::IBVQueuePair(
  ska::pst::recv::RDMACommunicationManager * cm,
  ska::pst::recv::IBVProtectionDomain * pd,
  ska::pst::recv::IBVCompletionQueue * cq,
  int n_slots
)
{
  ibv_qp_init_attr attr{nullptr};
  memset(&attr, 0, sizeof(attr));
  attr.send_cq = cq->get();
  attr.recv_cq = cq->get();
  attr.qp_type = IBV_QPT_RAW_PACKET;
  attr.cap.max_send_wr = 1;
  attr.cap.max_recv_wr = n_slots;
  attr.cap.max_send_sge = 1;
  attr.cap.max_recv_sge = 1;

  // create the QP
  SPDLOG_DEBUG("ska::pst::recv::IBVQueuePair::IBVQueuePair ibv_create_qp()");
  errno = 0;
  qp = ibv_create_qp(pd->get(), &attr);
  if (!qp)
  {
    SPDLOG_ERROR("ska::pst::recv::IBVQueuePair::IBVQueuePair ibv_create_qp failed errno={}", errno);
    throw std::runtime_error("ska::pst::recv::IBVQueuePair::IBVQueuePair ibv_create_qp failed");
  }

  // modify its state to init
  modify(IBV_QPS_INIT, cm->get()->port_num);
}

ska::pst::recv::IBVQueuePair::~IBVQueuePair()
{
  SPDLOG_TRACE("ska::pst::recv::IBVQueuePair::~IBVQueuePair");
  if (qp)
  {
    ibv_destroy_qp(qp);
  }
  qp = nullptr;
}

void ska::pst::recv::IBVQueuePair::modify(ibv_qp_state qp_state)
{
  ibv_qp_attr attr{};
  std::memset(&attr, 0, sizeof(attr));
  attr.qp_state = qp_state;
  modify(&attr, IBV_QP_STATE);
}

void ska::pst::recv::IBVQueuePair::modify(ibv_qp_state qp_state, int port_num)
{
  ibv_qp_attr attr{};
  std::memset(&attr, 0, sizeof(attr));
  attr.qp_state = qp_state;
  attr.port_num = port_num;
  modify(&attr, IBV_QP_STATE | IBV_QP_PORT);
}

void ska::pst::recv::IBVQueuePair::modify(ibv_qp_attr *attr, int attr_mask)
{
  int status = ibv_modify_qp(qp, attr, attr_mask);
  if (status != 0)
  {
    throw std::runtime_error("ska::pst::recv::IBVQueuePair::modify ibv_modify_qp failed");
  }
}

void ska::pst::recv::IBVQueuePair::post_recv(ibv_recv_wr *wr)
{
  ibv_recv_wr * bad_wr = nullptr;
  int status = ibv_post_recv(qp, wr, &bad_wr);
  if (status != 0)
  {
    throw std::runtime_error("ska::pst::recv::IBVQueuePair::post_recv ibv_post_recv failed");
  }
}

ska::pst::recv::IBVMemoryRegion::IBVMemoryRegion(IBVProtectionDomain * pd, void *addr, std::size_t length)
{
  SPDLOG_DEBUG("ska::pst::recv::IBVMemoryRegion::IBVMemoryRegion ibv_reg_mr()");
  mr = ibv_reg_mr(pd->get(), addr, length, IBV_ACCESS_LOCAL_WRITE); // NOLINT
  if (!mr)
  {
    throw std::runtime_error("ska::pst::recv::IBVMemoryRegion::IBVMemoryRegion ibv_reg_mr failed");
  }
}

ska::pst::recv::IBVMemoryRegion::~IBVMemoryRegion()
{
  SPDLOG_TRACE("ska::pst::recv::IBVMemoryRegion::~IBVMemoryRegion");
  if (mr)
  {
    ibv_dereg_mr(mr);
  }
  mr = nullptr;
}

ska::pst::recv::IBVFlow::IBVFlow(
  ska::pst::recv::RDMACommunicationManager * cm,
  ska::pst::recv::IBVQueuePair * qp,
  const std::string &ipv4_addr, int port)
{
  struct
  {
    ibv_flow_attr attr;
    ibv_flow_spec_eth eth __attribute__((packed));
    ibv_flow_spec_ipv4 ip __attribute__((packed));
    ibv_flow_spec_tcp_udp udp __attribute__((packed));
  } flow_rule{};

  memset(&flow_rule, 0, sizeof(flow_rule));

  flow_rule.attr.type = IBV_FLOW_ATTR_NORMAL;
  flow_rule.attr.priority = 0;
  flow_rule.attr.size = sizeof(flow_rule);
  flow_rule.attr.num_of_specs = 3;
  flow_rule.attr.port = cm->get()->port_num;

  /* At least the ConnectX-3 cards seem to require an Ethernet match. We
   * thus have to construct the Ethernet multicast address corresponding to
   * the IP multicast address from RFC 7042.
   */
  flow_rule.eth.type = IBV_FLOW_SPEC_ETH;
  flow_rule.eth.size = sizeof(flow_rule.eth);

  ska::pst::recv::mac_address dst_mac = ska::pst::recv::interface_mac(ipv4_addr);
  std::memcpy(&flow_rule.eth.val.dst_mac, &dst_mac, sizeof(dst_mac));

  // Set all 1's mask
  std::memset(&flow_rule.eth.mask.dst_mac, dst_mac_mask, sizeof(flow_rule.eth.mask.dst_mac));

  flow_rule.ip.type = IBV_FLOW_SPEC_IPV4;
  flow_rule.ip.size = sizeof(flow_rule.ip);

  ska::pst::recv::ip_addr_v4 dst_ip;
  inet_pton(AF_INET, ipv4_addr.c_str(), &dst_ip);
  std::memcpy(&flow_rule.ip.val.dst_ip, &dst_ip, sizeof(dst_ip));
  std::memset(&flow_rule.ip.mask.dst_ip, dst_ip_mask, sizeof(flow_rule.ip.mask.dst_ip));

  flow_rule.udp.type = IBV_FLOW_SPEC_UDP;
  flow_rule.udp.size = sizeof(flow_rule.udp);
  flow_rule.udp.val.dst_port = htobe16(port);
  flow_rule.udp.mask.dst_port = dst_port_mask;

  SPDLOG_DEBUG("ska::pst::recv::IBVQueue::create_flow flow rule created");
  flow = ibv_create_flow(qp->get(), &flow_rule.attr);
  if (!flow)
  {
    throw std::runtime_error("ska::pst::recv::IBVQueue::create_flow ibv_create_flow failed");
  }
}

ska::pst::recv::IBVFlow::~IBVFlow()
{
  SPDLOG_TRACE("ska::pst::recv::IBVFlow::~IBVFlow");
  if (flow)
  {
    ibv_destroy_flow(flow);
  }
  flow = nullptr;
}
