/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <arpa/inet.h>
#include <cstdlib>
#include <cerrno>
#include <stdexcept>
#include <spdlog/spdlog.h>

#include "ska/pst/recv/definitions.h"
#include "ska/pst/recv/network/UDPSocketReceive.h"

//! Global flag for receiving function
bool ska::pst::recv::SocketReceive::keep_receiving = true; // NOLINT

constexpr int ska::pst::recv::UDPSocketReceive::min_size;

ska::pst::recv::UDPSocketReceive::UDPSocketReceive()
{
  SPDLOG_TRACE("ska::pst::recv::UDPSocketReceive::UDPSocketReceive()");
  buf_ptr = get_buf(); // NOLINT
}

ska::pst::recv::UDPSocketReceive::~UDPSocketReceive()
{
  SPDLOG_TRACE("ska::pst::recv::UDPSocketReceive::~UDPSocketReceive()");
}

void ska::pst::recv::UDPSocketReceive::allocate_resources(size_t pref_size, size_t _packet_size)
{
  SPDLOG_DEBUG("ska::pst::recv::UDPSocketReceive::allocate_resources pref_size={} packet_size={}", pref_size, _packet_size);
  packet_size = static_cast<ssize_t>(_packet_size);

  // configure the socket buffer size
  SPDLOG_DEBUG("ska::pst::recv::UDPSocketReceive::configure_beam resize({})", packet_size);
  ska::pst::recv::Socket::resize(packet_size);
  buf_ptr = get_buf();

  kernel_socket_bufsz = pref_size;
}

void ska::pst::recv::UDPSocketReceive::configure(const std::string& ip_address, int _port)
{
  SPDLOG_DEBUG("ska::pst::recv::UDPSocketReceive::configure({}, {})", ip_address, _port);

  // open the socket FD
  ska::pst::recv::UDPSocket::open(_port);

  if (ip_address.compare("any") == 0)
  {
    udp_sock.sin_addr.s_addr = htonl(INADDR_ANY);
  }
  else
  {
    udp_sock.sin_addr.s_addr = inet_addr(ip_address.c_str());
    if (udp_sock.sin_addr.s_addr == -1)
    {
      SPDLOG_ERROR("ska::pst::recv::UDPSocketReceive::configure unable to interpret {} as valid IP address", ip_address);
      throw std::runtime_error("Unable to interpet IPv4 address");
    }
  }

  // bind socket to file descriptor
  if (::bind(fd, (struct sockaddr *) &udp_sock, sizeof(udp_sock)) == -1) // NOLINT
  {
    throw std::runtime_error("could not bind to UDP socket");
  }

  // all sockets non blocking by default
  set_nonblock();

  // attempt to set to the specified value
  int value = static_cast<int>(kernel_socket_bufsz);
  socklen_t len = sizeof(value);
  int retval = setsockopt(fd, SOL_SOCKET, SO_RCVBUF, &value, len);
  if (retval != 0)
  {
    SPDLOG_ERROR("ska::pst::recv::UDPSocketReceive setsockopt failed");
    throw std::runtime_error("could not set SO_RCVBUF to preferred size");
  }

  // now check if it worked
  value = 0;
  len = sizeof(value);
  retval = getsockopt(fd, SOL_SOCKET, SO_RCVBUF, &value, &len);
  if (retval != 0)
  {
    SPDLOG_ERROR("ska::pst::recv::UDPSocketReceive getsockopt failed");
    throw std::runtime_error("could not get SO_RCVBUF socket option value");
  }

  if (static_cast<int>(kernel_socket_bufsz * 2) != value)
  {
    SPDLOG_WARN("ska::pst::recv::UDPSocketReceive failed to set kernel buffer size to {}, kernel set it to {}", kernel_socket_bufsz, value);
  }
}

void ska::pst::recv::UDPSocketReceive::deconfigure_beam()
{
  close_me();
  reset_buffer();
}

auto ska::pst::recv::UDPSocketReceive::clear_buffered_packets() -> size_t
{
  const size_t bufsz = buf.size();
  size_t bytes_cleared = 0;
  ssize_t bytes_read = 0;
  unsigned keep_reading = 1;
  int errsv = 0;

  int was_blocking = get_blocking();
  SPDLOG_DEBUG("ska::pst::recv::UDPSocketReceive::clear_buffered_packets was_blocking={}", was_blocking);

  if (was_blocking)
  {
    SPDLOG_DEBUG("ska::pst::recv::UDPSocketReceive::clear_buffered_packets set_nonblock()");
    set_nonblock();
  }

  while (keep_reading && ska::pst::recv::UDPSocketReceive::keep_receiving)
  {
    SPDLOG_TRACE("ska::pst::recv::UDPSocketReceive::clear_buffered_packets recv_from({}, {}, {})", fd, reinterpret_cast<void *>(buf_ptr), bufsz);
    bytes_read = recvfrom(fd, buf_ptr, bufsz, 0, nullptr, nullptr);
    SPDLOG_TRACE("ska::pst::recv::UDPSocketReceive::clear_buffered_packets bytes_read={}", bytes_read);

    // packet was read
    if ((bytes_read > 0) && (bytes_read <= static_cast<ssize_t>(bufsz)))
    {
      bytes_cleared += bytes_read;
    }
    // no packets or an error has occurred
    else
    {
      keep_reading = 0;
      if (errno != EAGAIN)
      {
        SPDLOG_WARN("ska::pst::recv::UDPSocketReceive::clear_buffered_packets recv_from returned an error whilst clearing packets");
      }
    }
  }

  if (was_blocking)
  {
    set_block();
  }

  return bytes_cleared;
}

auto ska::pst::recv::UDPSocketReceive::acquire_packet() -> int
{
  #ifdef TRACE
  SPDLOG_TRACE("ska::pst::recv::UDPSocketReceive::recv_from");
  #endif
  const size_t bufsz = buf.size();
  if (have_packet)
  {
    return 0;
  }

  while (!have_packet && keep_receiving)
  {
    #ifdef TRACE
    SPDLOG_TRACE("ska::pst::recv::UDPSocketReceive::recv_from fd={} buf={} bufsz={}", fd, (void *) buf_ptr, bufsz);
    #endif
    ssize_t got = recvfrom(fd, buf_ptr, bufsz, 0, nullptr, nullptr);
    #ifdef TRACE
    SPDLOG_TRACE("ska::pst::recv::UDPSocketReceive::recv_from packet_size={}", packet_size);
    #endif
    if (got == packet_size)
    {
      have_packet = true;
      return 0;
    }
    else if (got > min_size)
    {
      SPDLOG_WARN("ska::pst::recv::UDPSocketReceive final packet malformed got={} packet_size={} min_size={}", got, packet_size, min_size);
      release_packet(0);
      return MALFORMED_PACKET;
    }
    else if (got == -1)
    {
      nsleeps++;
    }
    else
    {
      SPDLOG_WARN("ska::pst::recv::UDPSocketReceive error expected {} bytes, received {} bytes", packet_size, got);
      release_packet(0);
      return MALFORMED_PACKET;
    }
  }
  return NO_PACKET;
}

auto ska::pst::recv::UDPSocketReceive::get_buf_ptr(int slot_index) -> char *
{
  if (slot_index != 0)
  {
    throw std::runtime_error("ska::pst::recv::UDPSocketReceive::get_buf_ptr slot_index != 0");
  }
  return buf_ptr;
}

auto ska::pst::recv::UDPSocketReceive::process_sleeps() -> uint64_t
{
  uint64_t accumulated_sleeps = nsleeps;
  nsleeps = 0;
  return accumulated_sleeps;
}
