/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <fcntl.h>
#include <unistd.h>
#include <stdexcept>
#include <cstring>
#include <spdlog/spdlog.h>

#include "ska/pst/recv/network/Socket.h"

static constexpr size_t default_packet_buffer_size = 1500; // bytes

ska::pst::recv::Socket::Socket()
{
  buf.resize(default_packet_buffer_size);
  reset_buffer();
}

ska::pst::recv::Socket::~Socket()
{
  close_me();
}

void ska::pst::recv::Socket::close_me()
{
  if (fd > 0)
  {
    close(fd);
  }
  fd = 0;
}

void ska::pst::recv::Socket::resize(size_t new_bufsz)
{
  if (new_bufsz > buf.size())
  {
    SPDLOG_DEBUG("ska::pst::recv::Socket::resize increasing bufsz from {} to {}", buf.size(), new_bufsz);
    buf.resize(new_bufsz);
    reset_buffer();
  }
}

void ska::pst::recv::Socket::reset_buffer()
{
  std::fill(buf.begin(), buf.end(), 0);
}

auto ska::pst::recv::Socket::set_nonblock() -> int
{
  if (fd)
  {
    int flags{0};
    flags = fcntl(fd,F_GETFL); // NOLINT
    flags |= O_NONBLOCK;
    non_block = 1;
    return fcntl(fd,F_SETFL,flags); // NOLINT
  }
  else
  {
    throw std::runtime_error("socket was not open");
  }
}

auto ska::pst::recv::Socket::set_block() -> int
{
  if (fd)
  {
    int flags{0};
    flags = fcntl(fd,F_GETFL); // NOLINT
    flags &= ~(O_NONBLOCK);
    non_block = 0;
    return fcntl(fd,F_SETFL,flags); // NOLINT
  }
  else
  {
    throw std::runtime_error("socket was not open");
  }
}