/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <net/ethernet.h>
#include <net/if_arp.h>
#include <ifaddrs.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <linux/if_packet.h>
#include <stdexcept>
#include <spdlog/spdlog.h>

#include "ska/pst/recv/network/PacketUtils.h"

ska::pst::recv::PacketBuffer::PacketBuffer() :
  ptr(nullptr), length(0)
{
}

ska::pst::recv::PacketBuffer::PacketBuffer(char *_ptr, size_t size) :
  ptr(_ptr), length(size)
{
}

auto ska::pst::recv::PacketBuffer::data() const -> char *
{
  return ptr;
}

auto ska::pst::recv::PacketBuffer::size() const -> size_t
{
  return length;
}

constexpr std::size_t ska::pst::recv::EthernetFrame::min_size;

ska::pst::recv::EthernetFrame::EthernetFrame(char *_ptr, std::size_t size)
    : ska::pst::recv::PacketBuffer(_ptr, size)
{
  if (size < min_size)
  {
    throw std::length_error("packet is too small to be an ethernet frame");
  }
}

auto ska::pst::recv::EthernetFrame::payload_ipv4() const -> ska::pst::recv::IPv4Packet
{
  #ifdef DEBUG
  SPDLOG_DEBUG("ska::pst::recv::EthernetFrame::payload_ipv4 data()={} size={} min_size={}", (void *) data(), size(), min_size);
  #endif
  return ska::pst::recv::IPv4Packet(data() + min_size, size() - min_size); // NOLINT
}

constexpr uint16_t ska::pst::recv::IPv4Packet::ethertype;
constexpr size_t ska::pst::recv::IPv4Packet::min_size;
constexpr uint16_t ska::pst::recv::IPv4Packet::flag_do_not_fragment;
constexpr uint16_t ska::pst::recv::IPv4Packet::flag_more_fragments;

ska::pst::recv::IPv4Packet::IPv4Packet(char * _ptr, size_t size)
    : ska::pst::recv::PacketBuffer(_ptr, size)
{
  #ifdef DEBUG
  SPDLOG_DEBUG("ska::pst::recv::IPv4Packet::IPv4Packet _ptr={} size={}", (void *) _ptr, size);
  #endif
  if (size < min_size)
  {
    SPDLOG_ERROR("ska::pst::recv::IPv4Packet::IPv4Packet packet is too small for an IPv4 packet size={} min_size={}", size, min_size);
    throw std::length_error("packet is too small to be an IPv4 packet");
  }
}


auto ska::pst::recv::IPv4Packet::is_fragment() const -> bool
{
  // If either the more fragments flag is set, or we have a non-zero offset
  return flags_frag_off() & (flag_more_fragments | flag_more_fragments_bitmask);
}

auto ska::pst::recv::IPv4Packet::version() const -> int
{
  return version_ihl() >> 4;
}

auto ska::pst::recv::IPv4Packet::header_length() const -> size_t
{
  return 4 * (version_ihl() & header_length_bitmask);
}

auto ska::pst::recv::IPv4Packet::payload_udp() const -> ska::pst::recv::UDPPacket
{
  std::size_t h = header_length();
  std::size_t len = total_length();
  if (h > size() || h < min_size)
  {
    SPDLOG_ERROR("ska::pst::recv::IPv4Packet::payload_udp header_length={} size={} min_size={}", h, size(), min_size);
    throw std::length_error("ihl header is invalid");
  }
  if (len > size() || len < h)
  {
    SPDLOG_ERROR("ska::pst::recv::IPv4Packet::payload_udp total_length={} size={} min_size={}", len, size(), min_size);
    throw std::length_error("length header is invalid");
  }
  return ska::pst::recv::UDPPacket(data() + h, total_length() - h); // NOLINT
}

constexpr uint8_t ska::pst::recv::UDPPacket::protocol;
constexpr size_t ska::pst::recv::UDPPacket::min_size;

ska::pst::recv::UDPPacket::UDPPacket(char *_ptr, size_t size)
    : ska::pst::recv::PacketBuffer(_ptr, size)
{
  if (size < min_size)
  {
    throw std::length_error("packet is too small to be a UDP packet");
  }
}

auto ska::pst::recv::UDPPacket::payload() const -> ska::pst::recv::PacketBuffer
{
  size_t len = length();
  if (len > size() || len < min_size)
  {
    SPDLOG_ERROR("ska::pst::recv::UDPPacket::payload length={} size={} min_size={}", len, size(), min_size);
    throw std::length_error("length header is invalid");
  }
  return ska::pst::recv::PacketBuffer(data() + min_size, length() - min_size); // NOLINT
}

auto ska::pst::recv::udp_from_ethernet(char *ptr, size_t size) -> ska::pst::recv::PacketBuffer
{
  ska::pst::recv::EthernetFrame eth(ptr, size);
  if (eth.ethertype() != ska::pst::recv::IPv4Packet::ethertype)
  {
    SPDLOG_ERROR("ska::pst::recv::udp_from_ethernet Frame has wrong ethernet type (VLAN tagging?), discarding");
    throw std::runtime_error("Frame has wrong ethernet type (VLAN tagging?), discarding");
  }
  else
  {
    ska::pst::recv::IPv4Packet ipv4 = eth.payload_ipv4();
    if (ipv4.version() != 4)
    {
      SPDLOG_ERROR("ska::pst::recv::udp_from_ethernet Frame is not IPv4, discarding");
      throw std::runtime_error("Frame is not IPv4, discarding");
    }
    else if (ipv4.is_fragment())
    {
      SPDLOG_ERROR("ska::pst::recv::udp_from_ethernet IP datagram is fragmented, discarding");
      throw std::runtime_error("IP datagram is fragmented, discarding");
    }
    else if (ipv4.protocol() != ska::pst::recv::UDPPacket::protocol)
    {
      SPDLOG_ERROR("ska::pst::recv::udp_from_ethernet Packet is not UDP, discarding");
      throw std::runtime_error("Packet is not UDP, discarding");
    }
    else
    {
      return ipv4.payload_udp().payload();
    }
  }
}

namespace
{
  struct freeifaddrs_deleter
  {
    void operator()(ifaddrs *ifa) const { freeifaddrs(ifa); }
  };
} // anonymous namespace

auto ska::pst::recv::interface_mac(const std::string &ipv4_address) -> ska::pst::recv::mac_address
{
  ifaddrs *ifap{nullptr};
  if (getifaddrs(&ifap) < 0)
  {
    throw std::runtime_error("ska::pst::recv::interface_mac getifaddrs failed");
  }
  std::unique_ptr<ifaddrs, freeifaddrs_deleter> ifap_owner(ifap);

  // Map address to an interface name
  char *if_name = nullptr;
  for (ifaddrs *cur = ifap; cur; cur = cur->ifa_next)
  {
    //if (cur->ifa_addr && *(sa_family_t *) cur->ifa_addr == AF_INET)
    if (cur->ifa_addr && *(reinterpret_cast<sa_family_t *>(cur->ifa_addr)) == AF_INET)
    {
      const sockaddr_in *cur_address = reinterpret_cast<sockaddr_in *>(cur->ifa_addr);
      struct in_addr expected{};
      inet_pton(AF_INET, ipv4_address.c_str(), &expected);
      if (memcmp(&cur_address->sin_addr, &expected, sizeof(expected)) == 0)
      {
        if_name = cur->ifa_name;
        break;
      }
    }
  }
  if (!if_name)
  {
    throw std::runtime_error("no interface found with the address " + ipv4_address);
  }

  // Now find the MAC address for this interface
  static constexpr unsigned host_address_length = 6;
  for (ifaddrs *cur = ifap; cur; cur = cur->ifa_next)
  {
    if (std::strcmp(cur->ifa_name, if_name) == 0
        && cur->ifa_addr && *(reinterpret_cast<sa_family_t *>(cur->ifa_addr)) == AF_PACKET)
    {
      const sockaddr_ll *ll = reinterpret_cast<sockaddr_ll *>(cur->ifa_addr);
      if (ll->sll_hatype == ARPHRD_ETHER && ll->sll_halen == host_address_length)
      {
        mac_address mac;
        std::memcpy(&mac, ll->sll_addr, host_address_length); // NOLINT
        return mac;
      }
    }
  }
  throw std::runtime_error(std::string("no MAC address found for interface ") + if_name);
}
